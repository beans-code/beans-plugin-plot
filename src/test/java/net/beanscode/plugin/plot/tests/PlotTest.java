package net.beanscode.plugin.plot.tests;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.plot.Plot;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

public class PlotTest extends BeansTestCase {

	@Test
	public void testPlot() throws IOException, ValidationException {
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
				
		final User user = createTestUser("ut");
		
		final Dataset ds = new Dataset(user.getUserId(), "test pig notebook table")
				.save();
		addToRemove(ds);
		
		final int rowsMax = 100;
		final Table table = new Table(ds, "potential");
		table.getColumnDefs().add(new ColumnDef(DbObject.COLUMN_PK.toLowerCase(), null, ColumnType.LONG));
		table.getColumnDefs().add(new ColumnDef("tbid", null, ColumnType.STRING));
		table.getColumnDefs().add(new ColumnDef("c1", null, ColumnType.DOUBLE));
		table.getColumnDefs().add(new ColumnDef("c2", null, ColumnType.DOUBLE));
		table.getColumnDefs().add(new ColumnDef("c3", null, ColumnType.DOUBLE));
		table.save();
		
		CassandraConnector data = new CassandraConnector(table);
		double c2Sum = 0.0;
		double c3Sum = 0.0;
		for (long row = 0; row < rowsMax; row++) {
			
			Row r = new Row(UUID.random().getId());
			r.addColumn("tbid", table.getId().getId());
			r.addColumn("c1", 1.0 * row);
			r.addColumn("c2", 2.0 * row);
			r.addColumn("c3", 3.0 * row);
			data.write(r);
			
			c2Sum += r.getAsDouble("c2");
			c3Sum += r.getAsDouble("c3");
		}
		data.close();
		
		int rowsCounter = 0;
		double c2SumNew = 0.0;
		for (Row row : table.getDataIter()) {
			if (rowsCounter < 10)
				LibsLogger.debug(PlotTest.class, "row ", row);
			rowsCounter++;
			c2SumNew += (Double) row.get("c2");
		}
		assertTrue(rowsCounter == rowsMax, "Read " + rowsCounter + " rows but expected " + 123);
		assertTrue(c2Sum == c2SumNew, "Sum of the 2nd column does not match");
		
		runAllJobs(1000);
		
		Plot plot = new Plot();
//		plot.setText("TITLE \"line5\" "
//				+ "DATASETS \"" + ds.getId() + "\" "
//				+ "TABLES \"" + table.getName() + "\" "
//				+ "POINTS c1:c2");
//		plot.setFilename("tab5.pdf").plot();
	}
}
