package net.beanscode.plugin.plot.tests;

import java.io.File;
import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.tests.BeansTestCase;
//import net.beanscode.plugin.pig.PigScript;
import net.beanscode.plugin.plot.Plot;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class MOCCAGroupByPlotUnitTest extends BeansTestCase {

//	@Test
//	public void plot1() throws IOException, ValidationException {
////		SearchManager.searchInstance().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
//		
//		final User user = testUser();
//		final UUID userId = user.getUserId();
//		final UUID dsIdrbar100 = UUID.random();
//		final UUID dsIdrbar55 = UUID.random();
//		final UUID dsIdrbar35 = UUID.random();
//		final UUID notebookId = UUID.random();
//		final String tableName = "system";
//		final String notebookName = "MOCCA queries";
//		final String plotName = "plot1";
//		
//		// create objects
//		Dataset rbar100 = new Dataset().setId(dsIdrbar100).setName("MOCCA 600k rbar100").setUserId(userId).save();
//		Dataset rbar55 = new Dataset().setId(dsIdrbar55).setName("MOCCA 600k rbar55").setUserId(userId).save();
//		Dataset rbar35 = new Dataset().setId(dsIdrbar35).setName("MOCCA 600k rbar35").setUserId(userId).save();
//		
//		Table tab100 = new Table().setDatasetId(dsIdrbar100).setUserId(userId).setName(tableName).save();
//		Table tab55 = new Table().setDatasetId(dsIdrbar55).setUserId(userId).setName(tableName).save();
//		Table tab35 = new Table().setDatasetId(dsIdrbar35).setUserId(userId).setName(tableName).save();
//		
//		Notebook notebook = new Notebook().setId(notebookId).setUserId(userId).setName(notebookName).save();
//		
//		addToRemove(rbar100);
//		addToRemove(rbar55);
//		addToRemove(rbar35);
//		addToRemove(tab100);
//		addToRemove(tab55);
//		addToRemove(tab35);
//		addToRemove(notebook);
//		
//		// indexing objects
//		runAllJobs();
//		takeANap(1000);
//		
//		// importing data to the tables
//		tab100.importFile(new File(SystemUtils.getResourceURL("testdata/mocca/n600000/rbar100/rplum10/system.dat").getPath()));
//		tab55.importFile(new File(SystemUtils.getResourceURL("testdata/mocca/n600000/rbar55/rplum10/system.dat").getPath()));
//		tab35.importFile(new File(SystemUtils.getResourceURL("testdata/mocca/n600000/rbar35/rplum10/system.dat").getPath()));
//		
//		// test query
//		PigScript q = new PigScript(user, notebook.getId(), "query demo");
//		q.setQuery("rows = load '600k rbar100/system' using Table();"
//				+ "store rows into 'plot1/TYPE points "
//				+ "						COLUMNS tphys:smt "
//				+ "						TITLE \"Mass of the cluster\"' "
//				+ "										using Plot();");
//		q.save();
//		addToRemove(q);
//		
//		q.runQuery(false);
//		
////		// test data points
////		assertTrue(QueryStatus.getStatus(q.getId()).getStatus() == StatusType.DONE);
////		List<PlotData> dataPoints = PlotData.getPlotData(q.getId(), plotName);
////		System.out.println("mass    MV");
////		for (PlotData row : dataPoints) {
////			System.out.println(row.getColumn("mass") + "  " + row.getColumn("MV"));
////		}
////		
////		// save data points to the file
////		File dataPointsFile = FileUtils.createTmpFile();
////		PlotData.savePlotDataStream(q.getId(), plotName, new FileOutputStream(dataPointsFile, false), "tphys", "smt");
////		
////		// plotting results with manual gnuplot
////		new GnuplotFile(new File("test-harris-manual--mass-MV.pdf"))
////			.setTitle("Mass vs. absolute visual magnitude (MV) from Harris catalogue")
////			.addPlot(new Plot()
////						.setLegend("mass:MV")
////						.setInputFile(dataPointsFile))
////			.plot();
////		
//		// ploting results with cassandra object
//		net.beanscode.plugin.plot.Plot plot = ((Plot) NotebookEntryFactory.getNotebookEntry(q.getId()));
//		plot.setFilename("test-systemDat--mass.pdf").plot();
//		
////		// clear objects
////		dsHarris.remove();
////		tabRplum100.remove();
////		notebook.remove();
////		runAllJobs();
////		takeANap(1000);
////		
////		// check if Cass and ES is really empty
////		Dataset dsFromDb = Dataset.getDataset(userId, datasetId);
////		assertTrue(dsFromDb == null, "Dataset should be removed");
////		dsList = BeansSearchManager.searchDatasets("Harris catalogue");
////		assertTrue(dsList.size() == 0, "There should be no dataset in the index: " + dsList.size());
////		assertTrue(BeansSearchManager.searchNotebooks(notebookName).size() == 0);
//	}
}
