package net.beanscode.plugin.plot.tests;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.cass.DataUtils;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.plot.Plot;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.user.User;

public class PlotDataUnitTest extends BeansTestCase {
	
	public PlotDataUnitTest() {
		super();
	}

	@Test
	public void testPlot() throws IOException, ValidationException {
		User user = createTestUser("test");
		Dataset ds = createDataset(user, "datasetName");
		Table table = createTable(ds, "tableName");
		final UUID queryId = UUID.random();
		final String plotName = "test-name";
		
		final Notebook notebook = new Notebook();
		
		Plot plot = new Plot(notebook);//, "TYPE points COLUMNS x:y COLOR BY tbid TITLE \"x vs. y\"");
//		plot.getTypes().put("tbid", ColumnType.STRING);
//		plot.getTypes().put("x", ColumnType.DOUBLE);
//		plot.getTypes().put("y", ColumnType.DOUBLE);
		plot.save();
		
		plot = ((Plot) NotebookEntryFactory.getNotebookEntry(queryId));
		
		CassandraConnector data = new CassandraConnector(table);
		
		final int nrTables = 3;
		final int nrPoints = 10;
		
		for (int tableCounter = 1; tableCounter <= nrTables; tableCounter++) {
			for (int x = 0; x < nrPoints; x++) {
				data.write(new Row(UUID.random().getId())
								.addColumn("tbid", "tb" + tableCounter)
								.addColumn("x", (double) x)
								.addColumn("y", (Math.pow(x, (double) tableCounter))));
			}
		}
		
		data.close();
		
//		plot.setFilename("test-plot-colorby.pdf").plot();
		
		
		// checking if PlotData are correctly removed
		plot.remove();
		runAllJobs();
		assertTrue(DataUtils.isPlotEmpty(plotName), "PlotData should be empty for plot " + plotName);
	}
}
