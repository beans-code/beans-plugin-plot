package net.beanscode.plugin.plot.tests;

import org.junit.Test;

import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.tests.BeansTestCase;
//import net.beanscode.plugin.pig.PigScript;
import net.beanscode.plugin.plot.Plot;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.user.User;

public class TablePlotsUnitTest extends BeansTestCase {
	
//	@Test
//	public void testPlotColorBy() throws Exception {
//		final User user = testUser();
//		final Dataset dataset01 = createDataset(user, "fracb=0.1");
//		final Dataset dataset05 = createDataset(user, "fracb=0.5");
//		final Notebook notebook = createNotebook(user, "3k queries");
//		
//		CassandraConnector.importTableCassBulk(createTable(dataset01, "system"), SystemUtils.getResourceAsStream("testdata/mocca/n=3000/fracb=0.1/system.dat.beans"));
//		CassandraConnector.importTableCassBulk(createTable(dataset05, "system"), SystemUtils.getResourceAsStream("testdata/mocca/n=3000/fracb=0.5/system.dat.beans"));
//		
//		runAllJobs(1000);
//		
//		PigScript q = new PigScript(user, notebook.getId(), "smt vs. time");
//		q.setQuery("rows = LOAD '*/system' using MultiTable();"
//				+ "store rows into 'plot1/TYPE points "
//				+ "						COLUMNS tphys:smt "
//				+ "						COLOR BY tbid "
//				+ "						TITLE \"Mass of the cluster for 2 simulations\"' "
//				+ "										using Plot();");
//		q.save();
//		q.runQuery(false);
//		addToRemove(q);
//		
//		runAllJobs(1000);
//		
//		// ploting results with cassandra object
//		Plot plot = ((Plot) NotebookEntryFactory.getNotebookEntry(q.getId()));
//		plot.setFilename("test-system--mass-colorby.pdf").plot();
//	}
	
//	@Test
//	public void testPlotMultiTable() throws Exception {		
//		final User user = testUser();
//		final Dataset dataset01 = createDataset(user, "fracb=0.1");
//		final Dataset dataset05 = createDataset(user, "fracb=0.5");
//		final Notebook notebook = createNotebook(user, "3k queries");
//		
////		ImportUtils.importTableCassBulk(createTable(dataset, "snapshot"), SystemUtils.getResourceAsStream("testdata/mocca/n=3000/fracb=0.1/snapshot.dat.beans"));
//		CassandraConnector.importTableCassBulk(createTable(dataset01, "system"), SystemUtils.getResourceAsStream("testdata/mocca/n=3000/fracb=0.1/system.dat.beans"));
//		CassandraConnector.importTableCassBulk(createTable(dataset05, "system"), SystemUtils.getResourceAsStream("testdata/mocca/n=3000/fracb=0.5/system.dat.beans"));
//		
//		runAllJobs(1000);
//		
//		PigScript q = new PigScript(user, notebook.getId(), "smt vs. time");
//		q.setQuery("rows = LOAD '*/system' using MultiTable();"
//				+ "store rows into 'plot1/TYPE points "
//				+ "						COLUMNS tphys:smt "
//				+ "						TITLE \"Mass of the cluster for 2 simulations\"' "
//				+ "										using Plot();");
//		q.save();
//		q.runQuery(false);
//		addToRemove(q);
//		
//		runAllJobs(1000);
//		
//		// ploting results with cassandra object
//		Plot plot = ((Plot) NotebookEntryFactory.getNotebookEntry(q.getId()));
//		plot.setFilename("test-system--mass.pdf").plot();
//	}
}
