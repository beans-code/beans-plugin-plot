package net.beanscode.plugin.plot.tests;

import java.io.IOException;

import net.beanscode.model.cass.DataUtils;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.plot.Plot;
import net.beanscode.plugin.plot.PlotPrepareJob;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class PlotUnitTest extends BeansTestCase {

	@Test
	public void testPlot() throws IOException, ValidationException {
		FileExt source1 = new FileExt("src/main/resources/testdata/line-1-10.json");
		PlainConnector source1Conn = new PlainConnector(source1);
		
		User user = createTestUser("test");
		Dataset ds = createDataset(user, "datasetName");
		Table table = createTable(ds, "tableName");
		final String plotName = "test-name";
		
		table.getConnectorList().addConnector(source1Conn);
		table.importFile(source1);
		
		final Notebook notebook = new Notebook();
		notebook.setName("notebook");
		notebook.setUserId(user.getUserId());
		notebook.save();
		
		runAllJobs();
		
		Plot plot = new Plot(notebook);//, "TITLE \"x vs. y\" DATASETS \"dataset\" TABLES \"table\" POINTS x:y ");
		plot.setName(plotName);
		plot.setUserId(user.getUserId());
//		plot.getPlotStatus().setStarted(true);
		plot.save();
		
		new PlotPrepareJob(plot.getId()).save();
		
		runAllJobs();
		
//		plot.setFilename("test-plot-plainconnector-line-1-10.pdf");
		
		plot.plot();
		
		// checking if PlotData are correctly removed
		plot.remove();
		runAllJobs();
		assertTrue(DataUtils.isPlotEmpty(plotName), "PlotData should be empty for plot " + plotName);
	}
}
