package net.beanscode.plugin.plot.tests;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.plugin.plot.Plot;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.user.User;

public class PlotColorByTest extends BeansTestCase {

	@Test
	public void testPlot() throws IOException, ValidationException {
//		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Dataset.COLUMN_FAMILY.toLowerCase());
//		SearchManager.searchInstance().clearIndexType(WeblibsConst.KEYSPACE_LOWERCASE, Table.COLUMN_FAMILY.toLowerCase());
				
		final User user = createTestUser("ut");
		
		final Dataset ds1 = new Dataset(user.getUserId(), "ds1")
				.save();
		addToRemove(ds1);
		Dataset ds2 = new Dataset(user.getUserId(), "ds2")
				.save();
		addToRemove(ds2);
		
		final Table table1 = new Table(ds1, "tab1");
		table1.getColumnDefs().add(new ColumnDef(DbObject.COLUMN_PK.toLowerCase(), null, ColumnType.LONG));
		table1.getColumnDefs().add(new ColumnDef("tbid", null, ColumnType.STRING));
		table1.getColumnDefs().add(new ColumnDef("c1", null, ColumnType.DOUBLE));
		table1.getColumnDefs().add(new ColumnDef("c2", null, ColumnType.DOUBLE));
		table1.save();
		
		final Table table2 = new Table(ds2, "tab2");
		table2.getColumnDefs().add(new ColumnDef(DbObject.COLUMN_PK.toLowerCase(), null, ColumnType.LONG));
		table2.getColumnDefs().add(new ColumnDef("tbid", null, ColumnType.STRING));
		table2.getColumnDefs().add(new ColumnDef("c1", null, ColumnType.DOUBLE));
		table2.getColumnDefs().add(new ColumnDef("c2", null, ColumnType.DOUBLE));
		table2.save();
		
		CassandraConnector data1 = new CassandraConnector(table1);
		CassandraConnector data2 = new CassandraConnector(table2);
		for (long row = 0; row < 20; row++) {
			
			Row r = new Row(UUID.random().getId());
			r.addColumn("tbid", table1.getId().getId());
			r.addColumn("c1", 1.0 * row);
			r.addColumn("c2", 2.0 * row);
			data1.write(r);
			
			r = new Row(UUID.random().getId());
			r.addColumn("tbid", table2.getId().getId());
			r.addColumn("c1", 1.0 * row);
			r.addColumn("c2", 3.0 * row);
			data2.write(r);
		}
		data1.close();
		data2.close();
		
		runAllJobs(1000);
		
		
		Plot plot = new Plot();
//		plot.setText("TITLE \"Test lines\" DATASETS \"ds\" TABLES \"tab\" LINES c1:c2 \"c1 [DSNAME($tbid)/TBNAME($tbid)]\" "
//				+ "COLOR BY tbid LABELS \"c1 [x]\", \"c2 [x]\"");
//		plot.setFilename("test-lines-ds1-ds2.pdf").plot();
		
	}
}
