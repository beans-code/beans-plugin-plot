package net.beanscode.plugin.plot.tests;

import java.io.File;
import java.io.IOException;
import java.util.List;

import net.beanscode.model.cass.Data;
import net.beanscode.model.cass.DataFactory;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookFactory;
import net.beanscode.model.tests.BeansTestCase;
//import net.beanscode.plugin.pig.PigScript;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.plot.Plot;
import net.hypki.libs5.plot.gnuplot.GnuplotFile;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class HarrisPlotUnitTest extends BeansTestCase {

//	@Test
//	public void plot1() throws IOException, ValidationException {
////		SearchManager.searchInstance().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
//		
//		final User user = testUser();
//		final UUID userId = user.getUserId();
//		final UUID datasetId = UUID.random();
//		final UUID tableId = UUID.random();
//		final String tableName = "Harris3";
//		final String notebookName = "Harris queries";
//		final String plotName = "plot1";
//		
//		// create objects
//		new Dataset()
//			.setId(datasetId)
//			.setName("Harris catalogue")
//			.setUserId(userId)
//			.save();
//		new Table()
//			.setId(tableId)
//			.setDatasetId(datasetId)
//			.setUserId(userId)
//			.setName(tableName)
//			.save();
//		new Notebook()
//			.setUserId(userId)
//			.setName(notebookName)
//			.save();
//		
//		LibsLogger.debug(HarrisPlotUnitTest.class, "Saved dataset " + datasetId);
//		LibsLogger.debug(HarrisPlotUnitTest.class, "Saved table " + tableId);
//		LibsLogger.debug(HarrisPlotUnitTest.class, "Saved notebook " + notebookName);
//		
//		// indexing objects
//		runAllJobs();
//		takeANap(1000);
//		
//		// searching dataset
//		List<Dataset> dsList = DatasetFactory.searchDatasets(user.getUserId(), "Harris catalogue", 0, 10).getObjects();
//		assertTrue(dsList.size() == 1);
//		
//		Dataset dsHarris = dsList.get(0);
//		addToRemove(dsHarris);
//		assertTrue(dsHarris.getId().equals(datasetId), "Retrieved from ES dataset " + dsHarris.getId());
//		
//		// searching table
//		List<Table> tabList = TableFactory.searchTables(user.getUserId(), datasetId, tableName, 0, 100).getObjects();
//		assertTrue(tabList.size() == 1);
//		
//		Table tabHarris3 = tabList.get(0);
//		addToRemove(tabHarris3);
//		assertTrue(tabHarris3.getId().equals(tableId), "Retrieved from ES table " + tabHarris3.getId());
//		
//		// searching notebook
//		List<Notebook> notebooks = NotebookFactory.searchNotebooks(user.getUserId(), notebookName, 0, 20).getObjects();
//		assertTrue(notebooks.size() == 1);
//		assertTrue(notebooks.get(0).getName().equals(notebookName));
//		
//		Notebook notebook = notebooks.get(0);
//		addToRemove(notebook);
//		
//		// importing data to the table
//		tabHarris3.importFile(new File(SystemUtils.getResourceURL("testdata/harris3.dat").getPath()));
//		
//		// test query
//		PigScript q = new PigScript(user, notebook.getId(), "query demo");
//		q.setQuery("rows = load 'Harris catalogue/Harris3' using Table();"
//				+ "store rows into 'plot1/TYPE points "
//				+ "						COLUMNS mass:MV "
//				+ "						TITLE \"Mass vs. absolute visual magnitude (MV) from Harris catalogue\"' "
//				+ "										using Plot();");
//		q.save();
//		addToRemove(q);
//		
//		q.runQuery(false);
//		
//		// test data points
////		assertTrue(QueryStatusSorted.getStatus(q.getId()).getStatus() == PigScriptStatus.DONE);
//		Data dataPoints = DataFactory.getData(WeblibsConst.KEYSPACE, q.getId().getId());
//		System.out.println("mass    MV");
//		for (Row row : dataPoints) {
//			System.out.println(row.getColumns().get("mass") + "  " + row.getColumns().get("MV"));
//		}
//		
//		// save data points to the file
//		File dataPointsFile = FileUtils.createTmpFile();
//		
//		// plotting results with manual gnuplot
//		new GnuplotFile(new File("test-harris-manual--mass-MV.pdf"))
//			.setTitle("Mass vs. absolute visual magnitude (MV) from Harris catalogue")
//			.addPlot(new Plot()
//						.setLegend("mass:MV")
//						.setInputFile(dataPointsFile))
//			.plot();
//		
//		// ploting results with cassandra object
//		net.beanscode.plugin.plot.Plot plot = ((net.beanscode.plugin.plot.Plot) NotebookEntryFactory.getNotebookEntry(q.getId()));
//		plot.setFilename("test-harris-cass--mass-MV.pdf").plot();
//		
//		// clear objects
//		dsHarris.remove();
//		tabHarris3.remove();
//		notebook.remove();
//		runAllJobs();
//		takeANap(1000);
//		
//		// check if Cass and ES is really empty
//		Dataset dsFromDb = DatasetFactory.getDataset(datasetId);
//		assertTrue(dsFromDb == null, "Dataset should be removed");
//		dsList = DatasetFactory.searchDatasets(user.getUserId(), "Harris catalogue", 0, 10).getObjects();
//		assertTrue(dsList.size() == 0, "There should be no dataset in the index: " + dsList.size());
//		assertTrue(NotebookFactory.searchNotebooks(user.getUserId(), notebookName, 0, 20).getObjects().size() == 0);
//	}
}
