package net.beanscode.plugin.plot;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import com.google.gson.JsonElement;

import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.dataset.MetaUtils;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookEntryStatus;
import net.beanscode.model.notebook.ReloadPropagator;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.math.MathUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.SearchManager;

public class PlotParallelCoordinatesEntry extends NotebookEntry {
	
	private static final int MAX_TABLES_PER_PLOT = 1000;
	
	public static final String PNG = "PNG";
	public static final String PDF = "PDF";
	public static final String PS = "PS";
	public static final String ASCII = "ASCII";
	
	private static final String META_DS_QUERY 		= "META_DS_QUERY";
	private static final String META_TB_QUERY 		= "META_TB_QUERY";
	private static final String META_PLOT_STATUS	= "META_PLOT_STATUS";
	private static final String META_READ_TABLES	= "META_READ_TABLES";
	private static final String META_TITLE 			= "META_TITLE";
	private static final String META_COLUMNS 		= "META_COLUMNS";
	private static final String META_LEGENDS		= "META_LEGENDS";
	private static final String META_MIN_VALUE		= "META_MIN_VALUE";
	private static final String META_MAX_VALUE		= "META_MAX_VALUE";
	
//	@Expose
//	private String text = null;
	
//	@Expose
//	private String dsQuery = null;
//	
//	@Expose
//	private String tbQuery = null;
	
//	@Expose
//	private PlotStatus plotStatus = null;
	
//	@Expose
//	private List<UUID> readTables = null;
	
//	@Expose
//	private String title = null;
	
//	@Expose
//	private String columns = null;
	
//	@Expose
//	private List<String> legends = null;
	
//	@Expose
//	private List<Object> minValue = null;
	
//	@Expose
//	private List<Object> maxValue = null;
	
	private PlotStatus plotStatus = null;
	
	private List<String> legends = null;
	
	public PlotParallelCoordinatesEntry() {
		
	}
	
	public PlotParallelCoordinatesEntry(Notebook notebook, String optionsToParse) {
		setNotebookId(notebook.getId());
//		setText(optionsToParse);
		setUserId(notebook.getUserId());
	}
	
	public PlotParallelCoordinatesEntry(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}
	
	@Override
	public String getShortDescription() {
		return "Plot parallell coordinates";
	}
	
	@Override
	public String getSummary() {
		return "Plot: " + getTitle();
	}
	
	@Override
	public void index() throws IOException {
		SearchManager.index(this);
	}
	
	@Override
	public String getName() {
		return getTitle();
	}
	
	@Override
	public boolean isReloadNeeded() {
		try {
			Set<UUID> newReadTables = new java.util.HashSet<>();
//			List<UUID> oldTablesId = new ArrayList<>();
//			if (getTableStats() != null) {
//				for (TableStats tableStats : getTableStats().values())
//					oldTablesId.add(tableStats.getTableId());
//			}
			
			for (Table table : TableFactory.searchTables(getUserId(), 
					getDsQuery(), 
					getTbQuery(), 
					0, 
					MAX_TABLES_PER_PLOT).getObjects()) {
				
				if (table.getColumnDefs().size() == 0) {
					LibsLogger.warn(PlotParallelCoordinatesEntry.class, "Table " + table.getId() + " does not have ColumnDefs specified, ignoring "
							+ "it from determining whether the Pig script should be reloaded");
					continue;
				}
				
				// check for new tables
				if (!getReadTables().contains(table.getId()))
					return true;
				
//				try {
//					final TableStats oldTableStats = getTableStats().get(table.getId());
//					final TableStats newTableStats = table.getTableStats();
//					if (oldTableStats != null
//							&& newTableStats != null
//							&& oldTableStats.equals(newTableStats) == false)
//						return true;
//				} catch (IOException e) {
//					LibsLogger.error(PigScript.class, "Cannot check if table " + table.getId() + " needs reload, skipping...", e);
//				}
				
				newReadTables.add(table.getId());
//				oldTablesId.remove(table.getId());
			}
			
			// check if the list of tables has changed
			if (getReadTables().size() != newReadTables.size())
				return true;
		} catch (Exception e) {
			LibsLogger.error(PlotParallelCoordinatesEntry.class, "Cannot determine whether the Plot needs an update, assuming it does not need an update", e);
			return false;
		}
		
		return false;
	}
	
	@Override
	public boolean isRunning() {
		return !getPlotStatus().isFinished();
	}
	
	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		getPlotStatus().setStarted(true); // TODO save()?
		
		new PlotPrepareJob(getId()).save();
		
		return ReloadPropagator.NONBLOCK;
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
		
		assertTrue(!nullOrEmpty(getNotebookId()), "Notebok ID is required for Plot class");
	}
	
	@Override
	public NotebookEntry save() throws ValidationException, IOException {
		setPlotStatus(getPlotStatus());
		setLegends(getLegends());
		
		updateReadTables();
		
		return super.save();
	}
	
	public static String getDefaultText() {
		return SystemUtils.readFileContent(PlotParallelCoordinatesEntry.class, "PlotDefaultText");
	}

//	private void parse() {
//		try {
//			String optionsToParse = getText();
//			
//			if (nullOrEmpty(optionsToParse))
//				return;
//			
//			// clearing previous settings
//			setTitle(null);
//			setPlotStatus(null);
//			
//			final Notebook notebook = getNotebook();
//			
//			String datasets = null;
//			String notebooks = null;
//			String tables = null;
//			
//			TrickyString ts = new TrickyString(optionsToParse.replaceAll("\\r", " ").replaceAll("\\n", " "));
//			int lastLength = 0;
//			while (!ts.isEmpty() || (ts.length() == lastLength)) {
//				lastLength = ts.length();
//				
//				String keyword = ts.checkNextAlphabeticalWord();
//				
//				if (nullOrEmpty(keyword)) {
//
//					throw new NotImplementedException("Cannot parse plot near: " + substringMax(ts.toString(), 0, 20));
//				
//				} else if (keyword.equalsIgnoreCase("title")) {
//					
//					String tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextMatch("[\\s]*\"([^\"]+)\"[\\s]*");
//					setTitle(RegexUtils.firstGroup("\"([^\"]+)\"", tmp));
//					
//				} else if (keyword.equalsIgnoreCase("datasets")) {
//					
//					String tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextMatch("[\\s]*\"([^\"]+)\"[\\s]*");
//					datasets = RegexUtils.firstGroup("\"([^\"]+)\"", tmp);
//					
//				} else if (keyword.equalsIgnoreCase("notebooks")) {
//					
//					String tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextMatch("[\\s]*\"([^\"]+)\"[\\s]*");
//					notebooks = RegexUtils.firstGroup("\"([^\"]+)\"", tmp);
//					
//				} else if (keyword.equalsIgnoreCase("tables")) {
//					
//					String tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextMatch("[\\s]*\"([^\"]+)\"[\\s]*");
//					tables = RegexUtils.firstGroup("\"([^\"]+)\"", tmp);
//					
//				} else if (keyword.matches("(POINTS|LINES|BOXES|PARALLEL)")) {
//					
//					String type = ts.getNextAlphabeticalWord();
//					
//					int nextKeywordLocation = RegexUtils.firstGroupLocation("(POINTS|LINES|BOXES|PARALLEL|COLOR|SPLIT|TITLE|DATASETS|NOTEBOOKS|TABLES|LABELS|SORT)", ts.toString(), Pattern.CASE_INSENSITIVE);
//					String tmp = nextKeywordLocation == -1 ? ts.getLine() : ts.toString().substring(0, nextKeywordLocation);// ts.getNextStringUntil(untilString, withUntilString)Match("[^]+");
//					ts = new TrickyString(nextKeywordLocation == -1 ? "" : ts.toString().substring(nextKeywordLocation));
//					
//					String label = firstGroup("\"([^\"]+)\"", tmp);
//					List<String> oneColumns = allGroups("([^:\\\"]+)", notEmpty(label) ? tmp.replace(label, "") : tmp);
//
//					PlotSeries cols = new PlotSeries();
//					cols.setPlotType(PlotType.parse(type != null ? type.toUpperCase() : null, PlotType.POINTS));
//					
//					for (String col : oneColumns) {
//						if (notEmpty(col))
//							cols.addColumnExpr(col.trim());
//					}
//					
//					if (notEmpty(label))
//						cols.setLegend(label.trim());
//					
////					getSeries().add(cols);
//					
//				} else if (keyword.equalsIgnoreCase("sort")) {
//
//					String tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextAlphabeticalWord();
////					getSortBy().add(tmp);
//					
//				} else if (keyword.equalsIgnoreCase("color")) {
//
//					String tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextAlphabeticalWord();
////					setColorBy(tmp);
//					
//				} else if (keyword.equalsIgnoreCase("split")) {
//
//					String tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextAlphabeticalWord();
//					tmp = ts.getNextAlphabeticalWord();
////					setSplitBy(tmp);
////					setType(tmp, ColumnType.UNKNOWN);
//					
//				} else if (keyword.equalsIgnoreCase("labels")) {
//
//					while (true) {
//						String tmp = ts.getNextMatch("\\\"[^\\\"]*\\\"");
////						getLabels().add(RegexUtils.firstGroup("\\\"([^\\\"]+)\\\"", tmp));
//						
//						if (ts.isEmpty())
//							break;
//						else if (ts.checkNextPrintableCharacter().equals(",")) {
//							ts.getNextPrintableCharacter();
//							continue;
//						} else
//							break;
//					}
//					
//				} else
//					throw new NotImplementedException("Cannot parse plot near: " + substringMax(ts.toString(), 0, 20));
//			}
//			
//			// search for tables
//			setDsQuery(datasets);
//			setTbQuery(tables);
//			datasets = MetaUtils.applyMeta(datasets, notebook.getMeta());
//			notebooks= MetaUtils.applyMeta(notebooks, notebook.getMeta());
//			tables = MetaUtils.applyMeta(tables, notebook.getMeta());
//			
////			setTables(BeansSearchManager.searchTables(getUserId(), 
////					(datasets != null ? datasets : "") + " " + (notebooks != null ? notebooks : ""), 
////					tables, 
////					0, 
////					MAX_TABLES_PER_PLOT).getObjects());
////			
////			assertTrue(getTables().size() > 0, "No tables were found for for the plot");
//
////			// default colors
////			for (String col : getAllColumns())
////				if (getColors().get(col) == null)
////					getColors().put(col, Color.random());
////			
////			// default sizes
////			for (String col : getAllColumns())
////				if (getSizes().get(col) == null)
////					getSizes().put(col, 1.0f);
////			
////			for (String col : getAllColumns()) {
////				getTypes().put(col, ColumnType.UNKNOWN);
////			}
//
//		} catch (Exception e) {
//			LibsLogger.error(PlotParallelCoordinatesEntry.class, "Cannot parse plot", e);
//			getPlotStatus().setFinished(true, "Cannot parse plot. Internal message: " + e.getMessage());
//		}
//	}

	public String getTitle() {
		return getMetaAsString(META_TITLE, null);
	}
	
	@Override
	public void setName(String name) {
		setTitle(name);
	}

	public void setTitle(String title) {
		setMeta(META_TITLE, title);
		super.setName(title);
	}

	public static PlotParallelCoordinatesEntry getPlot(UUID entryId) throws IOException {
		NotebookEntry entry = NotebookEntryFactory.getNotebookEntry(entryId);
		return entry instanceof PlotParallelCoordinatesEntry ? (PlotParallelCoordinatesEntry) entry : null;
//		return get(Plot.class, WeblibsConst.KEYSPACE, Plot.COLUMN_FAMILY, queryId.getId() + "-" + plotName, DbObject.COLUMN_PK, DbObject.COLUMN_DATA);
	}
	
	public Iterable<Table> iterateTables() {
		return TableFactory.iterateTables(getUserId(), getDsQuery(), getTbQuery());
	}
	
	private void updateReadTables() {
		getReadTables().clear();
		
		for (Table table : iterateTables()) {
			getReadTables().add(table.getId());
		}
	}
	
	public ConnectorList getDataIter() throws IOException {
//		return DataFactory.getData(getTables());
		ConnectorList connData = new ConnectorList(1);
		
		String datasets = getDsQuery();
		datasets = MetaUtils.applyMeta(datasets, getNotebook());
		datasets = datasets.replace("THIS_NOTEBOOK", getNotebookId().getId());
		
		String tables = getTbQuery();
		tables = MetaUtils.applyMeta(tables, getNotebook());
		
		for (Table t : TableFactory.iterateTables(getUserId(), datasets, tables)) {
			connData.addConnector(t.getConnectorList());
		}
		
//		for (UUID tbid : getReadTables()) {
//			Table t = TableFactory.getTable(tbid);
//			connData.addConnector(t.getConnectorList());
//		}
		return connData;
	}
		
//	private PlotParallelCoordinatesEntry saveToFile() throws IOException {
//		
//		assertTrue(getReadTables() != null && getReadTables().size() > 0, "No tables were found for the plot");
//		
//		Watch watch = new Watch();
//		ConnectorList plotData = getDataIter();
//		
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, FileWriter> sinksWriters = new QuadrupleMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, File> sinksFiles = new QuadrupleMap<>();
//		final HashMap<Object/*split*/, Figure> sinksFigures = new HashMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, C3Where> sinksFilters = new QuadrupleMap<>();
//		final HashMap<Object/*split*/, String> sinksTitle = new HashMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, String> sinksLegends = new QuadrupleMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, boolean[]> sinksNeedsComputing = new QuadrupleMap<>();
//		
////		List<String> pdfFiles = new ArrayList<>();
//		
//		
//		final List<Object> splitKeys = new ArrayList<>();
//		if (getSplitBy() != null)
//			splitKeys.addAll(plotData.getUniqueValues(getSplitBy()));
//		else
//			splitKeys.add(null);
//		
//		final List<Object> colors = new ArrayList<>();
//		if (getColorBy() != null)
//			colors.addAll(plotData.getUniqueValues(getColorBy()));
//		else
//			colors.add(null);
//		
////		// check if only one plot was requested
////		if (plotNr >= 0) {
////			Object toPlot = plotNr < splitKeys.size() ? splitKeys.get(plotNr) : null;
////			
////			if (plotNr > 0 && plotNr >= splitKeys.size()) {
////				LibsLogger.error(Plot.class, "Plot nr " + plotNr + " was requested from Plot ID= " + getId() + " but there are only " + splitKeys + " SPLIT keys");
////				getPlotStatus().setMaxPlots(splitKeys.size());
////				return this;
////			}
////			
////			splitKeys.clear();
////			splitKeys.add(toPlot);
////		}
//		
//		final int fontsize = BeansSettings.getSettingAsInt(getUserId(), BeansSettings.SETTING_PLOT_FONT_SIZE, 10);
//		int plotNr = -1;
//		for (Object split : splitKeys) {
//			plotNr++;
//			
////			File tmpPdf = outputFile;
//			
////			if (splitKeys.size() > 1) {
////				tmpPdf = FileUtils.createTmpFile("beans", ".pdf");
////				pdfFiles.add(tmpPdf.getAbsolutePath());
////			}
//			
////			String title = getTitle() != null ? getTitle() : "";
////			
////			C3Where splitFilter = null;
////			if (split != null) {
////				splitFilter = new C3Where()
////					.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split));
////			}
////			
////			Row firstRow = plotData
////					.filter(splitFilter)
////					.iterator()
////					.next();
////			
////			title = applyParams(title, firstRow);
//			
//			
//			
//			
//			
//			
//			
//			for (Object color : colors) {
//				for (int i = 0; i < getSeries().size(); i++) {
//					PlotSeries columns = getSeries().get(i);
//					File tmpData = FileUtils.createTmpFile();
//					
//					Figure gnuplot = new GnuplotFile(new File(getFilenameTemplate().replace("PLOTNR", String.valueOf(plotNr))))
//							.setImageWidth(800)
//							.setImageHeight(480)
//							.setSmoothUnique(getSortBy() != null && getSortBy().size() > 0 ? true : false) // it adds sorting by gnuplot
//							.setTitle(getTitle())
//							.setFontsizeKey(fontsize)
//							.setXLabel(getLabels() != null && getLabels().size() > 0 ? getLabels().get(0) : null)
//							.setYLabel(getLabels() != null && getLabels().size() > 1 ? getLabels().get(1) : null)
//							;
//					sinksFigures.put(split, gnuplot);
//					sinksTitle.put(split, null);
//					
//					sinksFiles.put(split, color, i, tmpData);
//					sinksWriters.put(split, color, i, new FileWriter(tmpData));
//					
//					C3Where filter = new C3Where();
//					if (split != null && color != null) {
//						filter
//							.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split))
//							.addClause(new C3Clause(getColorBy(), ClauseType.EQ, color));
//					} else if (color != null) {
//						filter
//							.addClause(new C3Clause(getColorBy(), ClauseType.EQ, color));
//					} else if (split != null && color == null) {
//						filter
//							.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split));
//					} else {
//						filter = null;
//					}
//					sinksFilters.put(split, color, i, filter);
//					
//					sinksLegends.put(split, color, i, null);
//					
//					boolean [] needComputing = new boolean[columns.getColumnsSize()];
//					int k = 0;
//					for (String columnExpr : columns.iterateColumnExpr()) {
//						needComputing[k++] = (!columnExpr.matches("[\\w]+"));
//					}
//					sinksNeedsComputing.put(split, color, i, needComputing);
//				}
//			}	
//			
//		}
//		
//		for (Row row : plotData) {
//			for (Object split : splitKeys) {
//				for (Object color : colors) {
//					for (int i = 0; i < getSeries().size(); i++) {
//						PlotSeries columns = getSeries().get(i);
//						FileWriter fw = sinksWriters.get(split, color, i);
//						
//						// writing to a file
//						C3Where filters = sinksFilters.get(split, color, i);
//						boolean[] needComputing = sinksNeedsComputing.get(split, color, i);
//						if (filters == null || filters.size() == 0 || row.isFullfiled(filters, false)) {
//							// building title with the first row
//							if (sinksTitle.get(split) == null) {
//								String titleTmp = getTitle() != null ? getTitle() : "";
//								titleTmp = ApplyParams.applyParams(titleTmp, row);
//								sinksTitle.put(split, titleTmp);
//							}
//							
//							// building legend with the first row
//							if (sinksLegends.get(split, color, i) == null) {
//								String legend = columns.getLegend();
//								legend = legend == null ? "" : legend;
//								legend = ApplyParams.applyParams(legend, row);
//								sinksLegends.put(split, color, i, legend);
//								
//								Figure gnuplot = sinksFigures.get(split);
//								gnuplot = gnuplot.addPlot(new net.hypki.libs5.plot.Plot()
//									.setPlotType(columns.getPlotType())
//									.setSize(0.5)
//									.setLegend(legend)
//									.setInputFile(sinksFiles.get(split, color, i))
//									);
//								
//								// write header in the form: # column1  column2....
//								fw.append("# ");
//								for (String col : columns.iterateColumnExpr()) {
//									fw.append(col);
//									fw.append(" \t ");
//								}
//								fw.append("\n");
//							}
//							
//							int kk = 0;
//							for (String columnExpr : columns.iterateColumnExpr()) {
//								if (needComputing[kk++]) {
//									Object o = compute(columnExpr, columns.getParams(), row);
//									fw.append(o != null ? o.toString() : "NaN");
//								} else
//									fw.append(row.get(columnExpr).toString());
//								fw.append(" ");
//							}
//							fw.append("\n");
//						}
//					}
//				}
//			}
//		}
//		
//		// closing writers
//		for (Object split : splitKeys)
//			for (Object color : colors)
//				for (int i = 0; i < getSeries().size(); i++) {
//					sinksWriters.get(split, color, i).close();
//					LibsLogger.debug(PlotParallelCoordinatesEntry.class, "Sink writer ", sinksFiles.get(split, color, i), " closed");
//				}
//		
//		// preparing plots
//		if (getPlotType(0) == PlotType.LINES
//				|| getPlotType(0) == PlotType.POINTS)
//			for (Object split : splitKeys) {
//				Figure gnuplot = sinksFigures.get(split);
//				
//				gnuplot.setTitle(sinksTitle.get(split));
//				
//				gnuplot.plot();
//			}
//		
////		if (pdfFiles.size() > 0) {
////			PdfUtils.mergePdfs(pdfFiles, outputFile.getAbsolutePath());
////		}
//		
//		LibsLogger.debug(PlotParallelCoordinatesEntry.class, "Plot done in ", watch);
//		return this;
//	}

	private Object compute(final String expr, final List<String> params, final Row row) {
		String tmp = expr;
		for (String param : params) {
			Object o = row.get(param.startsWith("$") ? param.substring(1) : param);
			if (o != null)
				tmp = tmp.replace(param, new BigDecimal(String.valueOf(o)).toPlainString());

		}
		return MathUtils.eval(tmp);
	}
	
	public boolean containsColumn(String colName) {
		return getColumnsAsList().contains(colName);
	}

//	public String getText() {
//		if (text != null)
//			return text;
//		else {
//			StringBuilder sb = new StringBuilder();
//			sb.append("TITLE " + getTitle() + "\n");
//			sb.append("DATASETS \"" + getDsQuery() + "\" ");
//			sb.append("TABLES \"" + getTbQuery() + "\"\n");
//			sb.append("COLUMNS \"" + getColumns() + "\"\n");
//			return sb.toString();
//		}
//	}

//	public void setText(String text) {
//		this.text = text;
//		parse();
//	}
	
	public PlotStatus getPlotStatus() {
		if (plotStatus == null) {
			String tmp = getMetaAsString(META_PLOT_STATUS, null);
			if (nullOrEmpty(tmp)) {
				plotStatus = new PlotStatus();
				setPlotStatus(plotStatus);
			} else { 
				plotStatus = JsonUtils.fromJson(tmp, PlotStatus.class);
			}
		}
		return plotStatus;
	}

	private void setPlotStatus(PlotStatus plotStatus) {
		setMeta(META_PLOT_STATUS, JsonUtils.objectToStringPretty(plotStatus));
		this.plotStatus = plotStatus;
	}

	public String getDsQuery() {
		return getMetaAsString(META_DS_QUERY, null);
	}

	public void setDsQuery(String dsQuery) throws IOException {
		setMeta(META_DS_QUERY, dsQuery);
	}

	private void parseTables() throws IOException, ValidationException {
		String datasets = getDsQuery();
		String tables = getTbQuery();
		
		datasets = MetaUtils.applyMeta(datasets, getNotebook());
		tables = MetaUtils.applyMeta(tables, getNotebook());
		
		List<UUID> tablesids = getReadTables(); // TODO ugly
		tablesids.clear();
		for (Table table : TableFactory.iterateTables(getUserId(), datasets, tables)) {
			tablesids.add(table.getId());
		}
		setReadTables(tablesids);
	}
	
	public String getTbQuery() {
		return getMetaAsString(META_TB_QUERY, null);
	}

	public void setTbQuery(String tbQuery) {
		setMeta(META_TB_QUERY, tbQuery);
	}
	
	public void plot() throws IOException, ValidationException {
		try {
			assertTrue(StringUtilities.notEmpty(getDsQuery()), "Ds Query cannot be empty");
			assertTrue(StringUtilities.notEmpty(getTbQuery()), "Tb Query cannot be empty");
			
			setPlotStatus(null);
			getPlotStatus().setStarted(true);
			
			parseTables();
						
			// gnuplot is no longer used
//			removeOldPlots();
//			saveToFile();
			
			getPlotStatus().setFinished(true);
			setStatus(NotebookEntryStatus.SUCCESS);
			
			save();
		} catch (Exception e) {
			getPlotStatus().setFinished(true, e.getMessage());
			setStatus(NotebookEntryStatus.FAIL);
			
			save();
			
			throw new IOException("Cannot prepare plot", e);
		}
	}

	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.plots.PlotParallelCoordinatesPanel";
	}


	public String getColumns() {
		return getMetaAsString(META_COLUMNS, null);
	}

	public PlotParallelCoordinatesEntry setColumns(String columns) {
		setMeta(META_COLUMNS, columns);
		return this;
	}

	public List<String> getColumnsAsList() {
		if (nullOrEmpty(getColumns()))
			return new ArrayList<String>();
		
		List<String> columns = new ArrayList<>();
		for (JsonElement c : JsonUtils.parseJson(getColumns()).getAsJsonArray())
			columns.add(c.getAsString());
		
		return columns;
	}

	private List<UUID> getReadTables() {
		List<UUID> readTables = null;
		String tbStr = getMetaAsString(META_READ_TABLES, null);
		if (nullOrEmpty(tbStr)) {
			readTables = new ArrayList<>();
		} else {
			readTables = JsonUtils.readList(tbStr, UUID.class);
		}
		return readTables;
	}

	private void setReadTables(List<UUID> readTables) {
		setMeta(META_READ_TABLES, JsonUtils.objectToStringPretty(readTables));
	}

	public List<String> getLegends() {
		if (this.legends == null) {
			String tbStr = getMetaAsString(META_LEGENDS, null);
			if (nullOrEmpty(tbStr)
					|| !JsonUtils.isJsonSyntax(tbStr)) { // TODO copy it
				legends = new ArrayList<>();
			} else {
				legends = JsonUtils.readList(tbStr, String.class);
			}
		}
		return legends;
	}

	public void setLegends(List<String> legends) {
		this.legends = legends;
		setMeta(META_LEGENDS, JsonUtils.objectToStringPretty(legends));
	}
	
	@Override
	public void stop() throws ValidationException, IOException {
		// TODO Auto-generated method stub	
	}
	
	public Object getMinValue(int i) {
		return i < getMinValue().size() ? getMinValue().get(i) : null;
	}
	
	public Object getMaxValue(int i) {
		return i < getMaxValue().size() ? getMaxValue().get(i) : null;
	}

	public List<Object> getMinValue() {
		List<Object> minValue = null;
		String tbStr = getMetaAsString(META_MIN_VALUE, null);
		if (nullOrEmpty(tbStr)) {
			minValue = new ArrayList<>();
		} else {
			minValue = JsonUtils.readList(tbStr, Object.class);
		}
		return minValue;
	}

	public void setMinValue(List<Object> minValue) {
		setMeta(META_MIN_VALUE, JsonUtils.objectToStringPretty(minValue));
	}

	public List<Object> getMaxValue() {
		List<Object> maxValue = null;
		String tbStr = getMetaAsString(META_MAX_VALUE, null);
		if (nullOrEmpty(tbStr)) {
			maxValue = new ArrayList<>();
		} else {
			maxValue = JsonUtils.readList(tbStr, Object.class);
		}
		return maxValue;
	}

	public void setMaxValue(List<Object> maxValue) {
		setMeta(META_MAX_VALUE, JsonUtils.objectToStringPretty(maxValue));
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public StreamingOutput getDataAsStream(String split, String type) throws IOException {
		return new StreamingOutput() {
	        public void write(OutputStream output) throws IOException, WebApplicationException {
	            try {
	            	String plotTitle = getTitle();
	        		final ConnectorList plotData = getDataIter();
	            	
					Query filter = null;
	            	Writer writer = new OutputStreamWriter(output);
					
//						INFO Example: 
//							writer.write("[ { \"vr\": 12, \"c\": 13, \"rc\": 8, \"rh\": 360, \"muV\": 175, \"MV\": 3821, \"th\": 11 }, { \"vr\": 22, \"c\": 23, \"rc\": 28, \"rh\": 560, \"muV\": 175, \"MV\": 13821, \"th\": 21 } ]");
					
					writer.write(format("{ \"title\" : \"%s\", ", plotTitle));

					writer.write(" \"data\" : [");
					
					int noValueErrorCount = 0;
					long rows = 0;
					for (Row row : plotData.iterateRows(filter)) { // , false)) {
						if (rows++ > 0)
							writer.write(",");
						else {
							// adding min and max values if needed
							if (getMinValue(0) != null
									&& StringUtilities.notEmpty("" + getMinValue(0))) {
								writer.write("{");
								
								int colCount = 0;
								for (String colName : getColumnsAsList()) {
									Object v = getMinValue(colCount);
									
									String leg = colCount < getLegends().size() ? getLegends().get(colCount) : "";
									if (StringUtilities.nullOrEmpty(leg))
										leg = colName;
									if (colCount++ > 0)
										writer.write(",");
									writer.write("\"");
									writer.write(leg);
									writer.write("\" : ");
									
									if (v instanceof String) {
										writer.write("\"");
										writer.write("" + v);
										writer.write("\"");
									} else
										writer.write("" + v);
								}
								
								writer.write("}, ");
								writer.write("{");
								
								colCount = 0;
								for (String colName : getColumnsAsList()) {
									Object v = getMaxValue(colCount);
									
									String leg = colCount < getLegends().size() ? getLegends().get(colCount) : "";
									if (StringUtilities.nullOrEmpty(leg))
										leg = colName;
									if (colCount++ > 0)
										writer.write(",");
									writer.write("\"");
									writer.write(leg);
									writer.write("\" : ");
									
									if (v instanceof String) {
										writer.write("\"");
										writer.write("" + v);
										writer.write("\"");
									} else
										writer.write("" + v);
								}
								
								writer.write("}, ");
							}
						}
						
						writer.write("{");
						
						int colCount = 0;
						for (String colName : getColumnsAsList()) {
							Object v = null;
							
							if (colName.indexOf('$') >= 0)
								v = row.compute(colName, "$");
							else
								v = row.get(colName);
							
							if (v == null) {
								if (++noValueErrorCount < 10)
									LibsLogger.error(PlotParallelCoordinatesEntry.class, "There is no value for column ", colName);
								if (noValueErrorCount > 1000)
									break;
//								continue;
								v = 0.0; // TODO what about other types?
							}
							
							String leg = colCount < getLegends().size() ? getLegends().get(colCount) : "";
							if (StringUtilities.nullOrEmpty(leg))
								leg = colName;
							if (colCount++ > 0)
								writer.write(",");
							writer.write("\"");
							writer.write(leg);
							writer.write("\" : ");
							
							if (v instanceof String) {
								writer.write("\"");
								writer.write(v.toString());
								writer.write("\"");
							} else
								writer.write(v.toString());
						}
						
						writer.write("}");
						
						if (noValueErrorCount > 1000)
							break;
					}
					
					if (rows == 0) {
						// there is no data at all, writing some dummy values
						writer.write("{\"x\" : 1}");
					}
					
					writer.write("] }");
					
					writer.flush();
					writer.close();
					LibsLogger.debug(PlotParallelCoordinatesEntry.class, "Rows ", rows, " in PARALLEL plot");
	            } catch (Throwable e) {
	            	LibsLogger.error(PlotParallelCoordinatesEntry.class, "Cannot prepare response", e);
	                throw new WebApplicationException(e);
	            }
	        }
	    };
	}
}
