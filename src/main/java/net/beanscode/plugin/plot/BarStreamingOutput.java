package net.beanscode.plugin.plot;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import net.beanscode.model.BeansConst;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.settings.ColorPalette;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.QuadrupleMap;
import net.hypki.libs5.utils.collections.UniqueList;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.utils.NumberUtils;

public class BarStreamingOutput implements StreamingOutput {
	
	private PlotHistogramEntry plot = null;
	
	private String plotNr = null;
	
	private ColorPalette palette = null;

	public BarStreamingOutput(PlotHistogramEntry plot, String plotNr, ColorPalette palette) {
		setPlot(plot);
		setPlotNr(plotNr);
		setPalette(palette);
	}
	
	public void write(OutputStream output) throws IOException, WebApplicationException {
        try {
        	Watch w = new Watch();
        	
        	
        	String plotTitle = getPlot().getTitle();
        	
        	final String colX = getPlot().getX();
        	String colY = getPlot().getY();
        	boolean yIsNumber = NumberUtils.isNumber(colY);
        	final String colorBy = getPlot().getColorBy();
        	final List splits = new UniqueList<>();
        	
        	// creating histogram triple-map, it will be usefull to get 0 if there is no value for a particualar bin
        	final QuadrupleMap<Object, Object, Integer, Double> histogramNew = new QuadrupleMap<Object, Object, Integer, Double>(0.0);
        	final QuadrupleMap<Object, Object, String, Double> histogramLRnew = new QuadrupleMap<Object, Object, String, Double>();
        	boolean xNeedsComputing = getPlot().getX().contains("$");//("[\\w]+");
        	boolean yNeedsComputing = getPlot().getY().contains("$");//matches("[\\w]+");
        	Double minXGlobal = null;
        	Double maxXGlobal = null;
        	
        	HashMap<String, String> colorToLegend = new HashMap<String, String>();
        	
        	double binWidth = getPlot().getBinWidth();
        	int maxBin = 0;
        	
        	if (nullOrEmpty(colY)) {
        		// default weight is 1.0
        		colY = "1.0";
        		yIsNumber = true;
        	}
        	
        		// reading histogram
        		H5Connector conn = new H5Connector(plot.getId(), plot.getHistogramFile());
        		
	        	// reading histogram data
        		long rows = 0;
        		for (Row row : conn.iterateRows(null)) {
        			Object split = row.get("split");
        			Object color = row.get("color");
        			Integer bin = row.getAsInt("bin");
        			Double value = row.getAsDouble("value");
        			Double L = row.getAsDouble("L");
        			Double R = row.getAsDouble("R");
        			
        			splits.add(split);
        			
        			histogramNew.put(split, color, bin, value);
        			
        			histogramLRnew.put(split, color, "L", L);
        			histogramLRnew.put(split, color, "R", R);
//        			histogramLRnew.put(split, color, "L", Math.min(histogramLRnew.get(split, color, "L"), L));
//        			histogramLRnew.put(split, color, "R", Math.max(histogramLRnew.get(split, color, "R"), R));
        			
        			if (minXGlobal == null) {
        				minXGlobal = L;
        				maxXGlobal = R;
        			}
	        		minXGlobal = Math.min(minXGlobal, L);// TODO this should be computed only for a given split
	        		maxXGlobal = Math.max(maxXGlobal, R);
	        		
					maxBin = Math.max(maxBin, bin);// TODO this should be computed only for a given split
					
					colorToLegend.put(String.valueOf(color), "" + color);
					
					rows++;
				}
        		LibsLogger.debug(BarStreamingOutput.class, "Read ", rows, " histogram rows");
//	    		if (getPlot().getSplitBy() != null) {
//	    			splits.addAll(plotData.getUniqueValues(getPlot().getSplitBy()));
//	    		
////	    			if (splits.size() > 0 && splits.get(0) instanceof Number)
////	    				Collections.sort(splits);
//	    			
//	    			AssertUtils.assertTrue(getPlotNr() < splits.size(), "No more data for plotNr= ", getPlotNr());
//	    		}    		
	        	
	        	// get proper 'split' based on plotNr
	        	final Object splitValue = getPlotNr();//splits.contains(getPlotNr()) ? splits.get(getPlotNr()) : (splits.size() > 0 ? splits.get(splits.size() - 1) : PlotHistogramEntry.NOSPLIT);
	        	LibsLogger.debug(BarStreamingOutput.class, "BOXES plot for split= ", splitValue);
	        	if (getPlot().getSplitBy() != null)
	        		plotTitle += " [" + getPlot().getSplitBy() + "=" + splitValue + "]";
	        	
//	        	Query filterSplitBy = null;
	        	if (splitValue != null) {
//	        		filterSplitBy = new Query();
//	        		filterSplitBy
//						.addTermEQ(getPlot().getSplitBy(), splitValue);
	        	}
	        	
//	        	// searching for xLeft and xRight boundaries for every color
//	        	int rowsFailed = 0;
//	        	for (Row row : plot.getDataIter().iterateRows(filterSplitBy != null ? filterSplitBy : null)) {
//	        		if (rowsFailed > 100) {
//	        			LibsLogger.error(BarStreamingOutput.class, "Over 100 rows failed to compute for histogram, "
//	        					+ "breaking the plot");
//	        			break;
//	        		}
//	        		
//	        		try {
//		        		Double x = null;
//		        		
//						if (xNeedsComputing) {
//							BigDecimal bc = row.compute(getPlot().getX());
//							x = bc != null ? bc.doubleValue() : null;
//						} else
//							x = row.getAsDouble(getPlot().getX());
//						
//						if (x != null) {
//			        		Object color = colorBy != null ? row.get(colorBy) : colX;
//			        		if (histogramLR.get(color) == null) {
//			        			histogramLR.put(color, "L", x);
//			        			histogramLR.put(color, "R", x);
//			            		minXGlobal = x;
//			            		maxXGlobal = x;
//			        		} else {
//				        		if (x < ((double) histogramLR.get(color, "L")))
//				        			histogramLR.put(color, "L", x);
//				        		if (x > ((double) histogramLR.get(color, "R")))
//				        			histogramLR.put(color, "R", x);
//				        		minXGlobal = Math.min(minXGlobal, x);
//				        		maxXGlobal = Math.max(maxXGlobal, x);
//		        			}
//						}
//	        		} catch (Throwable e) {
//	        			if (++rowsFailed < 10)
//	        				LibsLogger.error(BarStreamingOutput.class, "Computing histogram for row " + row + " failed", e);
//	        		}
//	        	}
	        	
	        	// computing bin width
	        	if (binWidth == 0.0) {
	        		binWidth = (maxXGlobal - minXGlobal) / 10.0; // 10 bins by default
	        	}
	        		
	//        	final List<String> params = getPlot().getSeries().get(0).getParams();
//				for (Row row : plotData.iterateRows(filterSplitBy != null ? filterSplitBy : null)) {
//					
//	//				Object x = DataUtils.compute(colX, params, row);
//	//				Object y = DataUtils.compute(colY, params, row);
//					
//					Double x = null;
//					Double weight = null;
//	
//					if (xNeedsComputing) {
//						BigDecimal bc = row.compute(getPlot().getX());
//						x = bc != null ? bc.doubleValue() : null;
//					} else
//						x = row.getAsDouble(getPlot().getX());
//					
//					if (yIsNumber)
//						weight = Double.parseDouble(colY);
//					else {
//						if (yNeedsComputing)
//							weight = row.compute(getPlot().getY()).doubleValue();
//						else
//							weight = row.getAsDouble(getPlot().getY());
//					}
//					
//					Object color = colorBy != null ? row.get(colorBy) : colX;
//					if (x != null && weight != null && color != null) {
////						int bin = (int) ((x - histogramLR.get(color, "L")) / binWidth);
//						int bin = (int) ((x - minXGlobal) / binWidth);
//						histogram.put(color, bin, ((double) histogram.get(color, bin)) + (double) weight);
//	//					binValues.add(x);
//						
//						maxBin = Math.max(maxBin, bin);
//					}
//				}
				
	        	// TODO muszę włączyć wyświetlanie tylko XXX binów, bo dla IMBH i mas to miałem 23 tys binów i przeglądarka
	        	// siadała przy takiej ilości danych
//				if (binValues.size() > 0 && binValues.get(0) instanceof Number)
//					Collections.sort(binValues);
//				
//				// histogram plots can handle only some limited number of bins
				if (maxBin > BeansConst.PLOT_MAX_BAR_COUNT) {
					LibsLogger.error(BarStreamingOutput.class, "There is too many bars (> ", maxBin , ") for a plot, "
							+ "browser will most likely not be able to plot that. Removing last bars...");
					
					// removing last bins
					maxBin = BeansConst.PLOT_MAX_BAR_COUNT;
				}
				
//				// prepare legend
//				for (Object colorValue : histogramNew.get(splitValue).keySet()) {
//					if (colorValue != null) {
//						String legend = "";//plotSeries.getLegend();
//						
//						Query filter = new Query();
//						if (getPlot().getSplitBy() != null && colorBy != null) {
//							filter
//								.addTermEQ(getPlot().getSplitBy(), splitValue)
//								.addTermEQ(getPlot().getColorBy(), colorValue);
//							legend += colorValue + " " + splitValue;
//						} else if (colorBy != null) {
//							filter
//								.addTermEQ(getPlot().getColorBy(), colorValue);
//							legend += colorValue;
//						} else if (getPlot().getSplitBy() != null && colorBy == null) {
//							filter
//								.addTermEQ(getPlot().getSplitBy(), splitValue);
//							legend += splitValue;
//						} else {
//							filter = null;
//						}
//						
//						// TODO OPTY
//						Row firstRow = plot.getDataIter()
//								.iterator(filter, 0, 0)
//								.next();
//	
//						legend = ApplyParams.applyParams(legend, firstRow);
//						
//						colorToLegend.put(colorValue.toString(), legend);
//					}
//				}
	        	
//	        	legend = "abc";
	        	
//        	} catch (Throwable t) {
//        		LibsLogger.error(BarStreamingOutput.class, "Cannot prepare data for histogram, returning no points", t);
//        		
//    			// writing the output data for nvd3 plot
//    			Writer writer = new OutputStreamWriter(output);
//    			writer.write(format("{ \"title\" : \"%s\", \"data\" : [ ", plotTitle));
//				writer.write("] }");
//				writer.flush();
//				writer.close();
//				
//				return;
//        	}
			
			// writing the output data for nvd3 plot
			Writer writer = new OutputStreamWriter(output);
			writer.write(format("{ \"title\" : \"%s\", \"data\" : [ ", plotTitle));
			
			int colorCounter = 0;
			for (Object color : histogramNew.get(splitValue).keySet()) {
				if (colorCounter > 0)
					writer.write(", ");
				
				writer.write(format("{ \"key\": \"%s\",\"color\" : \"#%s\", \"values\": [ ", 
						colorToLegend.get(color) != null ? colorToLegend.get(color) : (color != null ? color.toString() : ""), 
						getPalette().toCssColor(colorCounter)));
								
				boolean first = true;
				for (int bin = 0; 
						bin <= (int) ((maxXGlobal - minXGlobal)/binWidth + 1) && bin <= maxBin; 
						bin++) {
					if (!first)
						writer.write(",");
					
//					double x = histogramLR.get(color, "L") + binWidth * bin;
//					double x = histogramLRnew.get(splitValue, color, "L") + binWidth * bin;
					double x = minXGlobal + binWidth * bin;
					
					writer.write(format("{ \"x\": %s, \"y\": %s }", String.valueOf(x), 
							histogramNew.get(splitValue, color, bin)));
					first = false;
				}
				
				writer.write("] }");
				
				colorCounter++;
			}
			
			writer.write("] }");
			
			writer.flush();
			writer.close();
			
			LibsLogger.debug(BarStreamingOutput.class, "Total time ", w);
        } catch (Exception e) {
        	LibsLogger.error(BarStreamingOutput.class, "Cannot prepare response", e);
            throw new WebApplicationException(e);
        }
    }

	private void addMissingValues(List xUnique) {
		try {
			if (xUnique == null || xUnique.size() <= 2)
				return;
			
			if (!(xUnique.get(0) instanceof Number))
				return;
			
			List<Number> toAdd = new UniqueList<>();
			List<Number> numbers = (List<Number>) xUnique;
			Double minXGap = Double.MAX_VALUE;
			for (int i = 0; i < numbers.size() - 1; i++)
				minXGap = Math.min(minXGap, Math.abs(numbers.get(i).doubleValue() - numbers.get(i + 1).doubleValue()));
			
			for (int i = 0; i < numbers.size() - 1; i++) {
				Double gap = Math.abs(numbers.get(i).doubleValue() - numbers.get(i + 1).doubleValue());
				
				if (gap > minXGap) {
					int counter = 1;
					while (gap > minXGap) {
						toAdd.add(numbers.get(i).doubleValue() + (counter++) * minXGap);
						gap -= minXGap;
						
						if ((numbers.size() + toAdd.size()) > 5000) {
							LibsLogger.error(BarStreamingOutput.class, "Cannot put more than 10k bars into histogram");
							minXGap = Double.MAX_VALUE;
						}
					}
				}
			}
			
			numbers.addAll(toAdd);

			Collections.sort(numbers, new Comparator<Number>() {
				public int compare(Number o1, Number o2) {
					return o1.doubleValue() < o2.doubleValue() ? -1 : 1;
				};
			});
		} catch (Exception e) {
			LibsLogger.error(BarStreamingOutput.class, "Cannot add missing values", e);
		}
	}

	private PlotHistogramEntry getPlot() {
		return plot;
	}

	private void setPlot(PlotHistogramEntry plot) {
		this.plot = plot;
	}

	private String getPlotNr() {
		return plotNr;
	}

	private void setPlotNr(String plotNr) {
		this.plotNr = plotNr;
	}

	private ColorPalette getPalette() {
		return palette;
	}

	private void setPalette(ColorPalette palette) {
		this.palette = palette;
	}
}
