package net.beanscode.plugin.plot;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.notebook.NotebookEntryFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class PlotPrepareJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID entryId = null;

	public PlotPrepareJob() {
		
	}
	
	public PlotPrepareJob(UUID entryId) {
		setEntryId(entryId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			Plot plot = ((Plot) NotebookEntryFactory.getNotebookEntry(getEntryId()));
			if (plot != null) {
				plot.plot();
				return;
			}
			
			PlotHistogramEntry plotHist = ((PlotHistogramEntry) NotebookEntryFactory.getNotebookEntry(getEntryId()));
			if (plotHist != null) {
				plotHist.plot();
				return;
			}
			
			PlotParallelCoordinatesEntry plotPar = ((PlotParallelCoordinatesEntry) NotebookEntryFactory.getNotebookEntry(getEntryId()));			
			if (plotPar != null) {
				plotPar.plot();
				return;
			}
			
			LibsLogger.warn(PlotPrepareJob.class, "Plot ", getEntryId(), " does not exist anymore, consuming job...");
		} catch (Exception e) {
			LibsLogger.error(PlotPrepareJob.class, "Cannot prepare a plot, consuming job", e);
		}
	}

	public UUID getEntryId() {
		return entryId;
	}

	public void setEntryId(UUID entryId) {
		this.entryId = entryId;
	}
}
