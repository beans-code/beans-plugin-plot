package net.beanscode.plugin.plot;

import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.onClick;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.plugins.CallPlugin;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class PlotPlugin implements Plugin {

	public PlotPlugin() {
		
	}

	@Override
	public String getName() {
		return "Plot plugin";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}

	@Override
	public String getDescription() {
		return "Plot plugin gives basic functionalities to make plots in Notebooks based on the data from Datasets and Tables";
	}

	@Override
	public String getPanel(UUID userId) throws IOException {
		HtmlCanvas html = new HtmlCanvas();
		
		String fontsize = BeansSettings.getSettingAsString(userId, BeansSettings.SETTING_PLOT_FONT_SIZE, "size", "8");
		
		final UUID id = UUID.random();
		html
			.div(id(id.getId()))
				.p()
					.content("Plot plugin gives basic functionalities to make plots in Notebooks based on the data from Datasets and Tables")
				.div()
					.h3()
						.content("Font size for plots (default value is 8)")
					.input(id("plot-fontsize")
							.class_("plot-fontsize")
							.name("plot-fontsize")
							.value(fontsize))
					.br()
					.a(onClick(new CallPlugin(this, "onSave", "$('#" + id + "  :input').serialize()").toString()))
						.content("Save changes")
				._div()
			._div();
		
		return html.toHtml();
	}
	
	private OpList onSave(Params params) {
		final String userId = params.getString("userId");
		final long fontsize = params.getLong("plot-fontsize", 8);
		
		BeansSettings.setSetting(new UUID(userId), 
				BeansSettings.SETTING_PLOT_FONT_SIZE,
				"size",
				String.valueOf(fontsize));
		
		return new OpList()
			.add(new OpInfo("Settings saved"));
	}

	@Override
	public List<Func> getFuncList() {
		return null;
	}

	@Override
	public List<Class<? extends Connector>> getConnectorClasses() {
		return null;
	}

	@Override
	public List<Class<? extends NotebookEntry>> getNotebookEntries() {
		List<Class<? extends NotebookEntry>> entries = new ArrayList<>();
		entries.add(Plot.class);
		entries.add(PlotParallelCoordinatesEntry.class);
		entries.add(PlotHistogramEntry.class);
		return entries;
	}
	
	@Override
	public List<Class<? extends DatabaseProvider>> getDatabaseProviderList() {
		return null;
	}
	
	@Override
	public List<Class<? extends SearchEngineProvider>> getSearchEngineProviderList() {
		return null;
	}
	
	@Override
	public boolean selfTest(UUID userId, OutputStreamWriter output) throws ValidationException, IOException {
		output.write("TODO");
		return true;
	}
}
