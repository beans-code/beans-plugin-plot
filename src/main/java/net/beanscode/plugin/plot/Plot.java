package net.beanscode.plugin.plot;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.cli.notebooks.PlotSeries;
import net.beanscode.model.BeansConst;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.dataset.MetaUtils;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryStatus;
import net.beanscode.model.notebook.ReloadPropagator;
import net.beanscode.model.settings.ColorPalette;
import net.beanscode.model.utils.ApplyParams;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.plot.Color;
import net.hypki.libs5.plot.Figure;
import net.hypki.libs5.plot.PlotType;
import net.hypki.libs5.plot.Range;
import net.hypki.libs5.plot.gnuplot.GnuplotFile;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.QuadrupleMap;
import net.hypki.libs5.utils.collections.UniqueList;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.math.MathUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.SearchManager;

// TODO name to PlotEntry
public class Plot extends NotebookEntry {
	
//	private static final String META_DS_QUERY 		= "META_DS_QUERY";
//	private static final String META_NOTEBOOK_QUERY = "META_NOTEBOOK_QUERY";
//	private static final String META_TB_QUERY 		= "META_TB_QUERY";
//	private static final String META_PLOT_STATUS	= "META_PLOT_STATUS";
//	private static final String META_TABLES			= "META_TABLES";
//	private static final String META_TITLE			= "META_TITLE";
//	private static final String META_SERIES			= "META_SERIES";
//	private static final String META_LABELS			= "META_LABELS";
//	private static final String META_SORTBY			= "META_SORTBY";
//	private static final String META_LOGSCALE_ON 	= "META_LOGSCALE_ON";
//	private static final String META_COLORS 		= "META_COLORS";
//	private static final String META_SIZES 			= "META_SIZES";
//	private static final String META_TYPES 			= "META_TYPES";
//	private static final String META_COLORBY		= "META_COLORBY";
//	private static final String META_SPLITBY		= "META_SPLITBY";
//	private static final String META_XRANGE			= "META_XRANGE";
//	private static final String META_YRANGE			= "META_YRANGE";
//	private static final String META_BOXWIDTH		= "META_BOXWIDTH";
//	private static final String META_FILENAME_TEMPLATE		= "META_FILENAME_TEMPLATE";
//	private static final String META_LEGEND			= "META_LEGEND";
	private static final String META_READ_TABLES	= "META_READ_TABLES";
	
	private static final String META_FIGURE			= "FIGURE";
	
//	private PlotStatus plotStatus = null;
//	private List<PlotSeries> series = null;
//	private List<String> labels = null;
//	private List<String> sortBy = null;
//	private Map<String, Color> colors = null;
//	private Map<String, Float> sizes = null;
//	private Map<String, ColumnType> types = null;
//	private Range xRange = null;
//	private Range yRange = null;
//	private Map<String, String> legend = null;
//	private List<Table> tables = null;
	
	// cache
	private Figure figure = null;
	
	public Plot() {
		
	}
	
	public Plot(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}
	
	@Override
	public String getShortDescription() {
		return "Plot points/lines";
	}
	
	@Override
	public String getSummary() {
		return "Plot: " + getName();
	}
	
	@Override
	public void index() throws IOException {
//		Plot neTmp = JsonUtils.fromJson(getData(), Plot.class);
//		neTmp.setTables(null);
		
		SearchManager.index(this);
	}
	
	@Override
	public boolean isReloadNeeded() {
		// TODO implement it from scratch
//		try {
//			Set<UUID> newReadTables = new java.util.HashSet<>();
//			
//			for (Table table : TableFactory.iterateTables(getUserId(), 
//					(getDsQuery() != null ? getDsQuery() : "") + " " + (getNotebookQuery() != null ? getNotebookQuery() : ""), 
//					getTbQuery())) {
//				
//				if (table != null
//						&& table.getColumnDefs() != null
//						&& table.getColumnDefs().size() == 0) {
//					LibsLogger.warn(Plot.class, "Table " + table.getId() + " does not have ColumnDefs specified, ignoring "
//							+ "it from determining whether the Pig script should be reloaded");
//					continue;
//				}
//				
//				// check for new tables
//				if (!getReadTables().contains(table.getId()))
//					return true;
//				
//				newReadTables.add(table.getId());
//			}
//			
//			// check if the list of tables has changed
//			if (getReadTables().size() != newReadTables.size())
//				return true;
//		} catch (Exception e) {
//			LibsLogger.error(Plot.class, "Cannot determine whether the Plot needs an update, assuming it does not need an update", e);
//			return false;
//		}
		
		return false;
	}
	
//	private String getDsQuery() {
//		Meta m = getFigure().getPlots().get("")
//		return null;
//	}

	@Override
	public boolean isRunning() {
		try {
			return !getProgress().isFinished();
		} catch (IOException e) {
			LibsLogger.error(Plot.class, "Cannot check progress", e);
			return false;
		}
	}
	
	@Override
	public List<String> getOutputColumns() throws IOException {
		// TODO Auto-generated method stub
		return super.getOutputColumns();
	}
	
	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		getProgress()
			.running(0.0, "Running..")
			.save();
		
//		setLabels(null);
		
//		save();
		
		new PlotPrepareJob(getId()).save();
		
		return ReloadPropagator.NONBLOCK;
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
		
		assertTrue(!nullOrEmpty(getNotebookId()), "Notebok ID is required for Plot class");
	}
	
	public static String getDefaultText() {
		return SystemUtils.readFileContent(Plot.class, "PlotDefaultText");
	}
	
	public ConnectorList getDataIter() throws IOException {
		ConnectorList connData = new ConnectorList(1);
		if (getFigure().getPlots().size() > 0) {
			String source = getFigure().getPlots().get(0).getDataSource();
			String [] dsTb = source != null ? StringUtilities.split(source, "@@") : null;
			if (dsTb != null) {
				String datasets = dsTb[0];
				datasets = MetaUtils.applyMeta(datasets, getNotebook());
				
				String tables = dsTb[1];
				tables = MetaUtils.applyMeta(tables, getNotebook());
				
				for (Table t : TableFactory.iterateTables(getUserId(), datasets, tables)) {
					connData.addConnector(t.getConnectorList());
				}
			}
		}
		return connData;
	}
	
//	@Override
//	public NotebookEntry save() throws ValidationException, IOException {
//		setPlotStatus(getPlotStatus());
//		setSeries(getSeries());
//		setLabels(getLabels());
//		setSortBy(getSortBy());
//		setColors(getColors());
//		setSizes(getSizes());
//		setTypes(getTypes());
//		setLegend(getLegend());
//		
//		return super.save();
//	}
	
	@Override
	public NotebookEntry save() throws ValidationException, IOException {
		setMeta(META_FIGURE, JsonUtils.objectToString(getFigure()));
		
		return super.save();
	}
		
//	// ugly function
//	private Plot saveToFile() throws IOException {
//		
//		assertTrue(getTables() != null && getTables().size() > 0, "No tables were found for the plot");
//		
//		Watch watch = new Watch();
//		ConnectorList plotData = getDataIter();
//		
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, FileWriter> sinksWriters = new QuadrupleMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, File> sinksFiles = new QuadrupleMap<>();
//		final HashMap<Object/*split*/, Figure> sinksFigures = new HashMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, C3Where> sinksFilters = new QuadrupleMap<>();
//		final HashMap<Object/*split*/, String> sinksTitle = new HashMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, String> sinksLegends = new QuadrupleMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, boolean[]> sinksNeedsComputing = new QuadrupleMap<>();
//		
////		List<String> pdfFiles = new ArrayList<>();
//		
//		
//		final List<Object> splitKeys = new ArrayList<>();
//		if (getSplitBy() != null)
//			splitKeys.addAll(plotData.getUniqueValues(getSplitBy()));
//		else
//			splitKeys.add(null);
//		
//		final List<Object> colors = new ArrayList<>();
//		if (getColorBy() != null)
//			colors.addAll(plotData.getUniqueValues(getColorBy()));
//		else
//			colors.add(null);
//		
//		final int fontsize = BeansSettings.getSettingAsInt(getUserId(), BeansSettings.SETTING_PLOT_FONT_SIZE, "size", 10);
//		int plotNr = -1;
//		for (Object split : splitKeys) {
//			plotNr++;
//			
//			for (Object color : colors) {
//				for (int i = 0; i < getSeries().size(); i++) {
//					PlotSeries columns = getSeries().get(i);
//					File tmpData = FileUtils.createTmpFile();
//					
//					Figure gnuplot = new GnuplotFile(new File(getFilenameTemplate().replace("PLOTNR", String.valueOf(plotNr))))
//							.setImageWidth(800)
//							.setImageHeight(480)
//							.setSmoothUnique(getSortBy() != null && getSortBy().size() > 0 ? true : false) // it adds sorting by gnuplot
//							.setTitle(getName())
//							.setFontsizeLegend(fontsize)
//							.setXLabel(getLabels() != null && getLabels().size() > 0 ? getLabels().get(0) : null)
//							.setYLabel(getLabels() != null && getLabels().size() > 1 ? getLabels().get(1) : null)
//							;
//					sinksFigures.put(split, gnuplot);
//					sinksTitle.put(split, null);
//					
//					sinksFiles.put(split, color, i, tmpData);
//					sinksWriters.put(split, color, i, new FileWriter(tmpData));
//					
//					C3Where filter = new C3Where();
//					if (split != null && color != null) {
//						filter
//							.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split))
//							.addClause(new C3Clause(getColorBy(), ClauseType.EQ, color));
//					} else if (color != null) {
//						filter
//							.addClause(new C3Clause(getColorBy(), ClauseType.EQ, color));
//					} else if (split != null && color == null) {
//						filter
//							.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split));
//					} else {
//						filter = null;
//					}
//					sinksFilters.put(split, color, i, filter);
//					
//					sinksLegends.put(split, color, i, null);
//					
//					boolean [] needComputing = new boolean[columns.getColumnsSize()];
//					int k = 0;
//					for (String columnExpr : columns.iterateColumnExpr()) {
//						needComputing[k++] = (!columnExpr.matches("[\\w]+"));
//					}
//					sinksNeedsComputing.put(split, color, i, needComputing);
//				}
//			}	
//			
//		}
//		
//		for (Row row : plotData) {
//			for (Object split : splitKeys) {
//				for (Object color : colors) {
//					for (int i = 0; i < getSeries().size(); i++) {
//						PlotSeries columns = getSeries().get(i);
//						FileWriter fw = sinksWriters.get(split, color, i);
//						
//						// writing to a file
//						C3Where filters = sinksFilters.get(split, color, i);
//						boolean[] needComputing = sinksNeedsComputing.get(split, color, i);
//						if (filters == null || filters.size() == 0 || row.isFullfiled(filters, false)) {
//							// building title with the first row
//							if (sinksTitle.get(split) == null) {
//								String titleTmp = getName() != null ? getName() : "";
//								titleTmp = ApplyParams.applyParams(titleTmp, row);
//								sinksTitle.put(split, titleTmp);
//							}
//							
//							// building legend with the first row
//							if (sinksLegends.get(split, color, i) == null) {
//								String legend = columns.getLegend();
//								legend = legend == null ? "" : legend;
//								legend = ApplyParams.applyParams(legend, row);
//								sinksLegends.put(split, color, i, legend);
//								
//								Figure gnuplot = sinksFigures.get(split);
//								gnuplot = gnuplot.addPlot(new net.hypki.libs5.plot.Plot()
//									.setPlotType(columns.getPlotType())
//									.setPointSize(0.5)
//									.setLegend(legend)
//									.setDataSource(sinksFiles.get(split, color, i).getAbsolutePath())
//									);
//								
//								// write header in the form: # column1  column2....
//								fw.append("# ");
//								for (String col : columns.iterateColumnExpr()) {
//									fw.append(col);
//									fw.append(" \t ");
//								}
//								fw.append("\n");
//							}
//							
//							int kk = 0;
//							for (String columnExpr : columns.iterateColumnExpr()) {
//								if (needComputing[kk++]) {
//									Object o = compute(columnExpr, columns.getParams(), row);
//									fw.append(o != null ? o.toString() : "NaN");
//								} else
//									fw.append(row.get(columnExpr).toString());
//								fw.append(" ");
//							}
//							fw.append("\n");
//						}
//					}
//				}
//			}
//		}
//		
//		// closing writers
//		for (Object split : splitKeys)
//			for (Object color : colors)
//				for (int i = 0; i < getSeries().size(); i++) {
//					sinksWriters.get(split, color, i).close();
//					LibsLogger.debug(Plot.class, "Sink writer ", sinksFiles.get(split, color, i), " closed");
//				}
//		
//		// preparing plots
//		if (getPlotType(0) == PlotType.LINES
//				|| getPlotType(0) == PlotType.POINTS)
//			for (Object split : splitKeys) {
//				Figure gnuplot = sinksFigures.get(split);
//				
//				gnuplot.setTitle(sinksTitle.get(split));
//				
//				gnuplot.plot();
//			}
//		
//		LibsLogger.debug(Plot.class, "Plot done in ", watch);
//		return this;
//	}

	private Object compute(final String expr, final List<String> params, final Row row) {
		String tmp = expr;
		for (String param : params) {
			Object o = row.get(param.startsWith("$") ? param.substring(1) : param);
			if (o != null)
				tmp = tmp.replace(param, new BigDecimal(String.valueOf(o)).toPlainString());

		}
		return MathUtils.eval(tmp);
	}


//	public String getText() {
//		if (text != null)
//			return text;
//		else {
//			StringBuilder sb = new StringBuilder();
//			sb.append("TITLE " + getTitle() + "\n");
//			sb.append((isDatasetQuery() ? "DATASETS \"" : "NOTEBOOKS \"") + getDsQuery() + "\" ");
//			sb.append("TABLES \"" + getTbQuery() + "\"\n");
//			for (PlotSeries ps : getSeries()) {
//				sb.append(ps.getPlotType().toString() + " " + ps.getX() + ":" + ps.getY() + (ps.isLegendDefined() ? " \"" + ps.getLegend() + "\"" : "") + "\n");
//			}
//			boolean addNewLine = false;
//			if (isSortByDefined()) {
//				sb.append("SORT BY " + getSortByAsString() + " ");
//				addNewLine = true;
//			}
//			if (isColorByDefined()) {
//				sb.append("COLOR BY " + getColorBy() + " ");
//				addNewLine = true;
//			}
//			if (isSplitBySet()) {
//				sb.append("SPLIT BY " + getSplitBy() + " ");
//				addNewLine = true;
//			}
//			if (isLabelXDefined() || isLabelYDefined()) {
//				sb.append((addNewLine ? "\n" : "") + "LABELS ");
//				if (isLabelXDefined()) {
//					sb.append(" \"" + getLabels().get(0) + "\"");
//					if (isLabelYDefined())
//						sb.append(", \"" + getLabels().get(1) + "\"");
//				}
//			}
//			return sb.toString();
//		}
//	}

//	public void setText(String text) {
//		this.text = text;
//		parse();
//	}

	
//	public PlotStatus getPlotStatus() {
//		if (plotStatus == null) {
//			String tmp = getMetaAsString(META_PLOT_STATUS, null);
//			if (tmp == null) {
//				plotStatus = new PlotStatus();
//				setPlotStatus(plotStatus);
//			} else { 
//				plotStatus = JsonUtils.fromJson(tmp, PlotStatus.class);
//			}
//		}
//		return plotStatus;
//	}

//	private void setPlotStatus(PlotStatus plotStatus) {
//		this.plotStatus = plotStatus;
//		setMeta(META_PLOT_STATUS, JsonUtils.objectToStringPretty(plotStatus));
//	}

//	public boolean isSplitBySet() {
//		return StringUtilities.notEmpty(getSplitBy());
//	}

	private void parseTables() throws IOException, ValidationException {
		String datasets = getFigure().getMeta().getAsString("META_DS_QUERY");
//		String notebooks = getNotebookQuery();
		String tables = getFigure().getMeta().getAsString("META_TB_QUERY");
		
		datasets = MetaUtils.applyMeta(datasets, getNotebook());
		
//		notebooks = MetaUtils.applyMeta(notebooks, getNotebook().getMeta());
		tables = MetaUtils.applyMeta(tables, getNotebook());
		
		List<UUID> readTables = new ArrayList<>();
		for (Table table : TableFactory.iterateTables(getUserId(), 
				(datasets != null ? datasets : ""), 
				tables)) {
			readTables.add(table.getId());
		}
		
		setReadTables(readTables);
		save();
	}
	
		
	private void removeOldPlots() {
		for (int i = 0; i < 1000000; i++) {
			FileExt f = new FileExt(BeansConst.PLOT_CACHE_PATH + "/" + getId() + "-" + i + ".png");
			if (f.exists())
				f.delete();
			else {
				LibsLogger.debug(Plot.class, "Removed ", i, " old plots for Plot ", getId());
				return;
			}
		}
		
		LibsLogger.error(Plot.class, "Something is wrong probably in function removeOldPlots, it removed 1M files");
	}

	public void plot() throws IOException, ValidationException {
		try {
//			assertTrue(StringUtilities.notEmpty(getDsQuery()), "Ds Query cannot be empty");
//			assertTrue(StringUtilities.notEmpty(getTbQuery()), "Tb Query cannot be empty");
			
			getProgress()
				.running(0, "Starting...")
				.save();
			
//			setPlotStatus(null);
//			getPlotStatus().setStarted(true);
			
//			if (StringUtilities.nullOrEmpty(getFilenameTemplate()))
//				setFilename(BeansConst.PLOT_CACHE_PATH + "/" + getId() + "-PLOTNR.png");
			
			parseTables();
						
			// gnuplot is no longer used
//			removeOldPlots();
//			saveToFile();
			
			getProgress()
				.ok()
				.save();
//			getPlotStatus().setFinished(true);
			setStatus(NotebookEntryStatus.SUCCESS);
			save();
		} catch (Exception e) {
			LibsLogger.error(Plot.class, "Cannot prepare plot", e);
			
			getProgress()
				.fail(e.getMessage())
				.save();
//			getPlotStatus().setFinished(true, e.getMessage());
			setStatus(NotebookEntryStatus.FAIL);
			save();
			
			throw new IOException("Cannot prepare plot", e);
		}
	}

//	public String getFilenameTemplate() {
//		return getMetaAsString(META_FILENAME_TEMPLATE, null);
//	}
//
//	public Plot setFilenameTemplate(File filename) {
//		setFilename(filename.getAbsolutePath());
//		return this;
//	}
//	
//	public Plot setFilename(String filename) {
//		setMeta(META_FILENAME_TEMPLATE, filename);
//		return this;
//	}
		
	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.plots.PlotPanel";
	}

	private List<UUID> getReadTables() {
		List<UUID> readTables = null;
		String tmp = getMetaAsString(META_READ_TABLES, null);
		if (tmp == null) {
			readTables = new ArrayList<>();
		} else {
			readTables = JsonUtils.readList(tmp, UUID.class);
		}
		return readTables;
	}

	private void setReadTables(List<UUID> readTables) {
		setMeta(META_READ_TABLES, JsonUtils.objectToStringPretty(readTables));
	}

	public String getLegend(Object color, Object split, net.hypki.libs5.plot.Plot series) {
		String tmp = getFigure()
						.getMeta()
						.getAsString(series.getLegend() + "__" + color + (split != null ? "__" + split : ""));
		if (tmp != null)
			return tmp;
				
		try {
			Query filter = new Query(); 
			
			if (color != null)
				filter
					.addTermEQ(series.getColorBy(), color);
			if (getFigure().getMeta().containsMeta("META_SPLITBY") && split != null)
				filter
					.addTermEQ(series.getSplitBy(), split);
			
			if (filter.getTerms().size() > 0) {
				for (Row row : getDataIter().iterateRows(filter)) {
					String newLegend = ApplyParams.applyParams(series.getLegend(), row);
					getFigure()
						.getMeta()
						.add(series.getLegend() + "__" + color + (split != null ? "__" + split : ""), newLegend);
//					save();
					return newLegend;
				}
			} else
				return series.getLegend();
		} catch (IOException e) {
			LibsLogger.error(Plot.class, "Cannot iterate over Plot data with filter", e);
		}
		return String.valueOf(color);
	}

	@Override
	public void stop() throws ValidationException, IOException {
		// do nothing
	}

	@Override
	public void start() {
		// do nothing
		try {
			getProgress()
				.ok()
				.save();
		} catch (ValidationException | IOException e) {
			LibsLogger.error(Plot.class, "Cannot save progress", e);
		}
	}
	
	public String getSplitBy() {
		return getFigure().getPlots().size() > 0 ? 
				getFigure().getPlots().get(0).getSplitBy() : null;
	}
	
	public String getColorBy() {
		return getFigure().getPlots().size() > 0 ? 
				getFigure().getPlots().get(0).getColorBy() : null;
	}
	
	public List<String> getSplits() {
		List<String> splits = new ArrayList<>();
		try {
			for (Object o : getDataIter().getUniqueValues(getSplitBy()))
				splits.add(String.valueOf(o));
		} catch (IOException e) {
			LibsLogger.error(Plot.class, "Cannot collect splits for plot", e);
		}
		return splits;
	}
	
	public InputStream getImageAsStream(String split) throws IOException {
		final FileExt file = new FileExt(BeansConst.PLOT_CACHE_PATH + "/" + getId() + "-" + split + ".png");
		return file.exists() ? new FileInputStream(file) : null;
	}
	
	@Override
	public StreamingOutput getDataAsStream(String split, String type) throws IOException {
		final PlotType plotType = getFigure().getPlots().get(0).getPlotType();
		final ConnectorList plotData = getDataIter();
		final DataType dataType = notEmpty(type) ? DataType.valueOf(type.toUpperCase()) : DataType.JSON;
		final ColorPalette palette = ColorPalette.getColorPalette(getUserId(), ColorPalette.defaultPalette());
		
		if (plotType == PlotType.LINES || plotType == PlotType.POINTS) {
			return new StreamingOutput() {
		        public void write(OutputStream output) throws IOException, WebApplicationException {
		            try {
		            	String plotTitle = getName();
		        		Writer writer = new OutputStreamWriter(output);
		        		
		        		List splits = new UniqueList<>();
		            	if (getSplitBy() != null) {
	            			splits.addAll(plotData.getUniqueValues(getSplitBy()));
	            			
	            			// TODO I just commented this
//	            			if (!getPlotStatus().isMaxPlotsSet()) {
//	            				getPlotStatus().setMaxPlots(splits.size());
//	            				save();
//	            			}
		            	}
		            	
		            	if (splits.size() > 0 && splits.get(0) instanceof Number)
		    				Collections.sort(splits);
		            	
		            	// get proper 'split' based on plotNr
//						final Object split = split;// plotNr < splits.size() ? splits.get(plotNr) : (splits.size() > 0 ? splits.get(splits.size() - 1) : null);
						LibsLogger.debug(Plot.class, "LINES/POINTS plot for split= ", split);
		            	

						Query splitClause = null;
		            	if (split != null) {
							plotTitle += " [" + getSplitBy() + "=" + split + "]";
							splitClause = new Query()
											.addTermEQ(getSplitBy(), split);
						}

		        		// TODO implement sorting again
//		        		if (plot.getSortBy() != null && plot.getSortBy().size() > 0) {
//		        			plotData.sort(plot.getSortBy().get(0));
//		        		}
						
		        		if (dataType == DataType.JSON) {
		        			writer.write(format("{ \"title\" : \"%s\", ", plotTitle));
		        			writer.write(" \"data\" : [");
		        		}
						
						int colorsCounter = 0;
						final List<Object> colors = new ArrayList<>();
						if (getColorBy() != null)
							colors.addAll(plotData.getUniqueValues(getColorBy(), getSplitBy(), split));
						else
							colors.add(null);
						
						if (dataType == DataType.PLAIN && colors.size() > 0) {
							writer.append("x, ");
							boolean first = true;
							boolean legendSet = false;
							for (Object color : colors) {
								for (net.hypki.libs5.plot.Plot series : getFigure().getPlots()) {
									if (!first)
										writer.append(", ");
									String legendTmp = getLegend(color, split, series);
									
									if (legendTmp == null)
										legendTmp = "no legend";
									
									legendTmp = legendTmp
											.replaceAll(",", "")
											.replaceAll("\"", "");
									writer.append(legendTmp);
//									writer.append(String.valueOf(color) + "--" + series.getY());
									first = false;
									legendSet = true;
									
//									LibsLogger.debug(PlotRest.class, "legend= ", legendTmp);
								}
							}
//							writer.append(StringUtilities.join(colors, ", "));
							writer.append("\n");
							
							if (legendSet)
								save();
						}
						
						int colorIdx = 0;
						String colorEmptyColsPrefix = "";
						String colorEmptyColsSufix = "";
						for (Object color : colors) {
							
							for (net.hypki.libs5.plot.Plot columns : getFigure().getPlots()) {
								
								colorEmptyColsPrefix = StringUtilities.repeat(",", colorIdx);
								colorEmptyColsSufix = StringUtilities.repeat(",", 
										colors.size() * getFigure().getPlots().size() - colorIdx - 1);
								
								if (dataType == DataType.JSON) {
									if (colorsCounter++ > 0)
										writer.write(", ");
									writer.write(" { \"values\" : [");
									
									boolean firstRow = true;
									Query where = new Query()
										.addTermEQ(getFigure().getMeta().getAsString("META_COLORBY"), color.toString());
									if (splitClause != null)
										where.addTerm(splitClause.getTerms().get(0));
									for (Row row : plotData.iterateRows(where)) {
										if (columns.getSample() < 1.0 && RandomUtils.nextDouble() > columns.getSample())
											continue;
										
										if (!firstRow) {
											writer.write(",");
											firstRow = false;
										}
										
										writer.write("{\"x\" : ");
										writer.write(row.get(columns.getX()).toString());
										writer.write(",\"y\" : ");
										writer.write(row.get(columns.getY()).toString());
										writer.write("}");
										
										firstRow = false;
									}
									
									writer.write("], ");
									if (color != null) {
										writer.write("\"key\": \"");
	//								if (!color.toString().equals(PlotData.NO_COLOR_BY)) {
										writer.write(color.toString());
										writer.write("\", ");
									}
									writer.write("\"color\": \"#");
									writer.write(palette.toCssColor(colorsCounter - 1));
									writer.write("\"} ");
								} else if (dataType == DataType.PLAIN) {
									boolean xNeedsComputing = columns.getX().contains("$");//("[\\w]+");
									boolean yNeedsComputing = columns.getY().contains("$");//matches("[\\w]+");
									int points = 0;
//									C3Where colorFilter = color == null ? null : 
//										new C3Where().addClause(new C3Clause(plot.getColorBy(), ClauseType.EQ, color));
									Query colorFilter = null;
									
									if (color != null)
										colorFilter = new Query()
											.addTermEQ(getColorBy(), color);

									if (splitClause != null) {
										if (colorFilter == null)
											colorFilter = new Query();
										colorFilter.addTerm(splitClause.getTerms().get(0));
									}
									
									for (Row row : plotData.iterateRows(colorFilter)) {
										if (columns.getSample() < 1.0 && RandomUtils.nextDouble() > columns.getSample())
											continue;
										
										Object x = null;
										Object y = null;

										if (xNeedsComputing)
											x = row.compute(columns.getX(), "$");
										else
											x = row.get(columns.getX());
										
										if (yNeedsComputing)
											y = row.compute(columns.getY(), "$");
										else
											y = row.get(columns.getY());
										
										// converting to scientific notation to avoid problems with dygraphs trying to use
										// the data as dates
										if (x != null && x instanceof BigDecimal)
											x = String.format("%e", x);
										if (y != null && y instanceof BigDecimal)
											y = String.format("%e", y);
//										if (RegexUtils.isInt(x.toString()))
//											x = Double.parseDouble(x.toString());
//										if (RegexUtils.isInt(y.toString()))
//											y = Double.parseDouble(y.toString());
										
										if (x != null && y != null) {
											writer.write(x.toString());
											writer.write(",");
											writer.write(colorEmptyColsPrefix);
											writer.write(y.toString());
											writer.write(colorEmptyColsSufix);
											writer.write("\n");
											points++;
											
//											LibsLogger.debug(PlotRest.class, "x ", x, " y= ", y);
										}
										
									}
									LibsLogger.debug(Plot.class, "Read ", points, " points for color= ", color);
								} else
									throw new NotImplementedException();
								
								colorIdx++;
								
							}
							
						}
						
						if (dataType == DataType.JSON)
							writer.write("] }");
						
						writer.flush();
						writer.close();
		            } catch (Throwable e) {
		            	LibsLogger.error(Plot.class, "Cannot prepare response", e);
		                throw new WebApplicationException(e);
		            }
		        }
		    };
//		} else if (plotType == PlotType.BOXES) {
////			return new BarStreamingOutput(plot, plotNr, palette);
//			return new BarStreamingOutput(null, plotNr, palette);
		} else {
			throw new IOException("Unimplemented case");
		}
	}
	
	public Figure getFigure() {
		if (figure == null) {
			this.figure = getMetaAsObject(META_FIGURE, Figure.class);
		}
		return figure;
	}
}
