package net.beanscode.plugin.plot;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.StreamingOutput;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.dataset.MetaUtils;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookEntryStatus;
import net.beanscode.model.notebook.ReloadPropagator;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.settings.ColorPalette;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.QuadrupleMap;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.math.MathUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.SearchManager;

public class PlotHistogramEntry extends NotebookEntry {
	

	public static final String NOSPLIT = "##";
	
	private static final int MAX_TABLES_PER_PLOT = 1000;
	
	public static final String PNG = "PNG";
	public static final String PDF = "PDF";
	public static final String PS = "PS";
	public static final String ASCII = "ASCII";
	
	private static final String META_DS_QUERY 		= "META_DS_QUERY";
	private static final String META_TB_QUERY 		= "META_TB_QUERY";
	private static final String META_PLOT_STATUS	= "META_PLOT_STATUS";
	private static final String META_READ_TABLES	= "META_READ_TABLES";
	private static final String META_TITLE			= "META_TITLE";
	private static final String META_XLABEL			= "META_XLABEL";
	private static final String META_YLABEL			= "META_YLABEL";
	private static final String META_SPLITBY		= "META_SPLITBY";
	private static final String META_COLORBY		= "META_COLORBY";
	private static final String META_X_COLUMN		= "META_X_COLUMN";
	private static final String META_Y_COLUMN		= "META_Y_COLUMN";
	private static final String META_BINWIDTH		= "META_BINWIDTH";
	
	// cache only
	private PlotStatus plotStatus = null;
	private List<UUID> readTables = null;
		
	public PlotHistogramEntry() {
		
	}
	
	public PlotHistogramEntry(Notebook notebook, String optionsToParse) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}
	
	public PlotHistogramEntry(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}
	
	@Override
	public String getShortDescription() {
		return "Plot histogram";
	}
	
	@Override
	public String getSummary() {
		return "Plot: " + getTitle();
	}
	
	@Override
	public void index() throws IOException {
		SearchManager.index(this);
	}
	
	@Override
	public String getName() {
		return getTitle();
	}
	
	@Override
	public boolean isReloadNeeded() {
		try {
			Set<UUID> newReadTables = new java.util.HashSet<>();
//			List<UUID> oldTablesId = new ArrayList<>();
//			if (getTableStats() != null) {
//				for (TableStats tableStats : getTableStats().values())
//					oldTablesId.add(tableStats.getTableId());
//			}
			
			for (Table table : TableFactory.searchTables(getUserId(), 
					getDsQuery(), 
					getTbQuery(), 
					0, 
					MAX_TABLES_PER_PLOT).getObjects()) {
				
				if (table.getColumnDefs().size() == 0) {
					LibsLogger.warn(PlotHistogramEntry.class, "Table " + table.getId() + " does not have ColumnDefs specified, ignoring "
							+ "it from determining whether the Pig script should be reloaded");
					continue;
				}
				
				// check for new tables
				if (!getReadTables().contains(table.getId()))
					return true;
				
//				try {
//					final TableStats oldTableStats = getTableStats().get(table.getId());
//					final TableStats newTableStats = table.getTableStats();
//					if (oldTableStats != null
//							&& newTableStats != null
//							&& oldTableStats.equals(newTableStats) == false)
//						return true;
//				} catch (IOException e) {
//					LibsLogger.error(PigScript.class, "Cannot check if table " + table.getId() + " needs reload, skipping...", e);
//				}
				
				newReadTables.add(table.getId());
//				oldTablesId.remove(table.getId());
			}
			
			// check if the list of tables has changed
			if (getReadTables().size() != newReadTables.size())
				return true;
		} catch (Exception e) {
			LibsLogger.error(PlotHistogramEntry.class, "Cannot determine whether the Plot needs an update, assuming it does not need an update", e);
			return false;
		}
		
		return false;
	}
	
	@Override
	public boolean isRunning() {
		return !getPlotStatus().isFinished();
	}
	
	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		getPlotStatus().setStarted(true); // TODO save()?
		
		new PlotPrepareJob(getId()).save();
		
		return ReloadPropagator.NONBLOCK;
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
		
		assertTrue(!nullOrEmpty(getNotebookId()), "Notebok ID is required for Plot class");
	}
	
	@Override
	public NotebookEntry save() throws ValidationException, IOException {
		setPlotStatus(getPlotStatus()); // it could change elsewhere, so save it to meta params to be sure
		
		updateReadTables();
		
		return super.save();
	}
	
	public static String getDefaultText() {
		return SystemUtils.readFileContent(PlotHistogramEntry.class, "PlotDefaultText");
	}

	public String getTitle() {
		return getMetaAsString(META_TITLE, null);
	}

	public void setTitle(String title) {
		setMeta(META_TITLE, title);
	}

	public static PlotHistogramEntry getPlot(UUID entryId) throws IOException {
		NotebookEntry entry = NotebookEntryFactory.getNotebookEntry(entryId);
		return entry instanceof PlotHistogramEntry ? (PlotHistogramEntry) entry : null;
//		return get(Plot.class, WeblibsConst.KEYSPACE, Plot.COLUMN_FAMILY, queryId.getId() + "-" + plotName, DbObject.COLUMN_PK, DbObject.COLUMN_DATA);
	}
	
	public Iterable<Table> iterateTables() {
		return TableFactory.iterateTables(getUserId(), getDsQuery(), getTbQuery());
	}
	
	private void updateReadTables() {
		getReadTables().clear();
		
		for (Table table : iterateTables()) {
			getReadTables().add(table.getId());
		}
	}
	
	public ConnectorList getDataIter() throws IOException {
//		return DataFactory.getData(getTables());
		ConnectorList connData = new ConnectorList(1);
		for (UUID tbid : getReadTables()) {
			Table t = TableFactory.getTable(tbid);
			if (t != null)
				connData.addConnector(t.getConnectorList());
		}
		return connData;
	}
		
//	private PlotParallelCoordinatesEntry saveToFile() throws IOException {
//		
//		assertTrue(getReadTables() != null && getReadTables().size() > 0, "No tables were found for the plot");
//		
//		Watch watch = new Watch();
//		ConnectorList plotData = getDataIter();
//		
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, FileWriter> sinksWriters = new QuadrupleMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, File> sinksFiles = new QuadrupleMap<>();
//		final HashMap<Object/*split*/, Figure> sinksFigures = new HashMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, C3Where> sinksFilters = new QuadrupleMap<>();
//		final HashMap<Object/*split*/, String> sinksTitle = new HashMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, String> sinksLegends = new QuadrupleMap<>();
//		final QuadrupleMap<Object/*split*/, Object/*color*/, Integer/*PlotSeries idx*/, boolean[]> sinksNeedsComputing = new QuadrupleMap<>();
//		
////		List<String> pdfFiles = new ArrayList<>();
//		
//		
//		final List<Object> splitKeys = new ArrayList<>();
//		if (getSplitBy() != null)
//			splitKeys.addAll(plotData.getUniqueValues(getSplitBy()));
//		else
//			splitKeys.add(null);
//		
//		final List<Object> colors = new ArrayList<>();
//		if (getColorBy() != null)
//			colors.addAll(plotData.getUniqueValues(getColorBy()));
//		else
//			colors.add(null);
//		
////		// check if only one plot was requested
////		if (plotNr >= 0) {
////			Object toPlot = plotNr < splitKeys.size() ? splitKeys.get(plotNr) : null;
////			
////			if (plotNr > 0 && plotNr >= splitKeys.size()) {
////				LibsLogger.error(Plot.class, "Plot nr " + plotNr + " was requested from Plot ID= " + getId() + " but there are only " + splitKeys + " SPLIT keys");
////				getPlotStatus().setMaxPlots(splitKeys.size());
////				return this;
////			}
////			
////			splitKeys.clear();
////			splitKeys.add(toPlot);
////		}
//		
//		final int fontsize = BeansSettings.getSettingAsInt(getUserId(), BeansSettings.SETTING_PLOT_FONT_SIZE, 10);
//		int plotNr = -1;
//		for (Object split : splitKeys) {
//			plotNr++;
//			
////			File tmpPdf = outputFile;
//			
////			if (splitKeys.size() > 1) {
////				tmpPdf = FileUtils.createTmpFile("beans", ".pdf");
////				pdfFiles.add(tmpPdf.getAbsolutePath());
////			}
//			
////			String title = getTitle() != null ? getTitle() : "";
////			
////			C3Where splitFilter = null;
////			if (split != null) {
////				splitFilter = new C3Where()
////					.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split));
////			}
////			
////			Row firstRow = plotData
////					.filter(splitFilter)
////					.iterator()
////					.next();
////			
////			title = applyParams(title, firstRow);
//			
//			
//			
//			
//			
//			
//			
//			for (Object color : colors) {
//				for (int i = 0; i < getSeries().size(); i++) {
//					PlotSeries columns = getSeries().get(i);
//					File tmpData = FileUtils.createTmpFile();
//					
//					Figure gnuplot = new GnuplotFile(new File(getFilenameTemplate().replace("PLOTNR", String.valueOf(plotNr))))
//							.setImageWidth(800)
//							.setImageHeight(480)
//							.setSmoothUnique(getSortBy() != null && getSortBy().size() > 0 ? true : false) // it adds sorting by gnuplot
//							.setTitle(getTitle())
//							.setFontsizeKey(fontsize)
//							.setXLabel(getLabels() != null && getLabels().size() > 0 ? getLabels().get(0) : null)
//							.setYLabel(getLabels() != null && getLabels().size() > 1 ? getLabels().get(1) : null)
//							;
//					sinksFigures.put(split, gnuplot);
//					sinksTitle.put(split, null);
//					
//					sinksFiles.put(split, color, i, tmpData);
//					sinksWriters.put(split, color, i, new FileWriter(tmpData));
//					
//					C3Where filter = new C3Where();
//					if (split != null && color != null) {
//						filter
//							.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split))
//							.addClause(new C3Clause(getColorBy(), ClauseType.EQ, color));
//					} else if (color != null) {
//						filter
//							.addClause(new C3Clause(getColorBy(), ClauseType.EQ, color));
//					} else if (split != null && color == null) {
//						filter
//							.addClause(new C3Clause(getSplitBy(), ClauseType.EQ, split));
//					} else {
//						filter = null;
//					}
//					sinksFilters.put(split, color, i, filter);
//					
//					sinksLegends.put(split, color, i, null);
//					
//					boolean [] needComputing = new boolean[columns.getColumnsSize()];
//					int k = 0;
//					for (String columnExpr : columns.iterateColumnExpr()) {
//						needComputing[k++] = (!columnExpr.matches("[\\w]+"));
//					}
//					sinksNeedsComputing.put(split, color, i, needComputing);
//				}
//			}	
//			
//		}
//		
//		for (Row row : plotData) {
//			for (Object split : splitKeys) {
//				for (Object color : colors) {
//					for (int i = 0; i < getSeries().size(); i++) {
//						PlotSeries columns = getSeries().get(i);
//						FileWriter fw = sinksWriters.get(split, color, i);
//						
//						// writing to a file
//						C3Where filters = sinksFilters.get(split, color, i);
//						boolean[] needComputing = sinksNeedsComputing.get(split, color, i);
//						if (filters == null || filters.size() == 0 || row.isFullfiled(filters, false)) {
//							// building title with the first row
//							if (sinksTitle.get(split) == null) {
//								String titleTmp = getTitle() != null ? getTitle() : "";
//								titleTmp = ApplyParams.applyParams(titleTmp, row);
//								sinksTitle.put(split, titleTmp);
//							}
//							
//							// building legend with the first row
//							if (sinksLegends.get(split, color, i) == null) {
//								String legend = columns.getLegend();
//								legend = legend == null ? "" : legend;
//								legend = ApplyParams.applyParams(legend, row);
//								sinksLegends.put(split, color, i, legend);
//								
//								Figure gnuplot = sinksFigures.get(split);
//								gnuplot = gnuplot.addPlot(new net.hypki.libs5.plot.Plot()
//									.setPlotType(columns.getPlotType())
//									.setSize(0.5)
//									.setLegend(legend)
//									.setInputFile(sinksFiles.get(split, color, i))
//									);
//								
//								// write header in the form: # column1  column2....
//								fw.append("# ");
//								for (String col : columns.iterateColumnExpr()) {
//									fw.append(col);
//									fw.append(" \t ");
//								}
//								fw.append("\n");
//							}
//							
//							int kk = 0;
//							for (String columnExpr : columns.iterateColumnExpr()) {
//								if (needComputing[kk++]) {
//									Object o = compute(columnExpr, columns.getParams(), row);
//									fw.append(o != null ? o.toString() : "NaN");
//								} else
//									fw.append(row.get(columnExpr).toString());
//								fw.append(" ");
//							}
//							fw.append("\n");
//						}
//					}
//				}
//			}
//		}
//		
//		// closing writers
//		for (Object split : splitKeys)
//			for (Object color : colors)
//				for (int i = 0; i < getSeries().size(); i++) {
//					sinksWriters.get(split, color, i).close();
//					LibsLogger.debug(PlotParallelCoordinatesEntry.class, "Sink writer ", sinksFiles.get(split, color, i), " closed");
//				}
//		
//		// preparing plots
//		if (getPlotType(0) == PlotType.LINES
//				|| getPlotType(0) == PlotType.POINTS)
//			for (Object split : splitKeys) {
//				Figure gnuplot = sinksFigures.get(split);
//				
//				gnuplot.setTitle(sinksTitle.get(split));
//				
//				gnuplot.plot();
//			}
//		
////		if (pdfFiles.size() > 0) {
////			PdfUtils.mergePdfs(pdfFiles, outputFile.getAbsolutePath());
////		}
//		
//		LibsLogger.debug(PlotParallelCoordinatesEntry.class, "Plot done in ", watch);
//		return this;
//	}

	private Object compute(final String expr, final List<String> params, final Row row) {
		String tmp = expr;
		for (String param : params) {
			Object o = row.get(param.startsWith("$") ? param.substring(1) : param);
			if (o != null)
				tmp = tmp.replace(param, new BigDecimal(String.valueOf(o)).toPlainString());

		}
		return MathUtils.eval(tmp);
	}

	public PlotStatus getPlotStatus() {
		if (plotStatus == null) {
			String tmp = getMetaAsString(META_PLOT_STATUS, null);
			if (tmp == null) {
				plotStatus = new PlotStatus();
				setPlotStatus(plotStatus);
			} else { 
				plotStatus = JsonUtils.fromJson(tmp, PlotStatus.class);
			}
		}
		return plotStatus;
	}

	private void setPlotStatus(PlotStatus plotStatus) {
		this.plotStatus = plotStatus;
		setMeta(META_PLOT_STATUS, JsonUtils.objectToStringPretty(plotStatus));
	}

	public String getDsQuery() {
		return getMetaAsString(META_DS_QUERY, null);
	}

	public void setDsQuery(String dsQuery) throws IOException {
		setMeta(META_DS_QUERY, dsQuery);
	}

	private void parseTables() throws IOException, ValidationException {
		String datasets = getDsQuery();
		String tables = getTbQuery();
		
		datasets = MetaUtils.applyMeta(datasets, getNotebook());
		tables = MetaUtils.applyMeta(tables, getNotebook());
		
		getReadTables().clear();
		for (Table table : TableFactory.iterateTables(getUserId(), datasets, tables)) {
			getReadTables().add(table.getId());
			setReadTables(getReadTables());
		}
	}
	
	public String getTbQuery() {
		return getMetaAsString(META_TB_QUERY, null);
	}

	public void setTbQuery(String tbQuery) {
		setMeta(META_TB_QUERY, tbQuery);
	}
	
	public void plot() throws IOException, ValidationException {
		try {
			assertTrue(StringUtilities.notEmpty(getDsQuery()), "Ds Query cannot be empty");
			assertTrue(StringUtilities.notEmpty(getTbQuery()), "Tb Query cannot be empty");
			
			setPlotStatus(null);
			getPlotStatus().setStarted(true);
			
			parseTables();
						
			// gnuplot is no longer used
//			removeOldPlots();
//			saveToFile();
			
			// preparing points
			prepareHistogram();
			
			getPlotStatus().setFinished(true);
			setStatus(NotebookEntryStatus.SUCCESS);
			save();
		} catch (Exception e) {
			getPlotStatus().setFinished(true, e.getMessage());
			setStatus(NotebookEntryStatus.FAIL);
			save();
			
			throw new IOException("Cannot prepare plot", e);
		}
	}

	private void prepareHistogram() throws IOException {
		try {
			// columns
	    	final String colX = getX();
	    	String colY = getY();
	    	boolean yIsNumber = NumberUtils.isNumber(colY);
	    	
	    	// split, color
	    	final String colorBy = getColorBy();
	    	final String splitBy = getSplitBy();
	    	final boolean isColorByDefined = StringUtilities.notEmpty(colorBy);
	    	final boolean isSplitByDefined = StringUtilities.notEmpty(splitBy);
	    	
	    	// histograms
	    	final QuadrupleMap<Object, Object, Integer, Double> histogram = new QuadrupleMap<Object, Object, Integer, Double>(0.0);
	    	final QuadrupleMap<Object, Object, String, Double> histogramLR = new QuadrupleMap<Object, Object, String, Double>();
	    	final boolean xNeedsComputing = getX().contains("$");
	    	final boolean yNeedsComputing = getY().contains("$");
	    	double minXGlobal = 0.0;
	    	double maxXGlobal = 0.0;
	    	
	    	// bins
	    	double binWidth = getBinWidth();
	    	int maxBin = 0;
	    	
	    	if (nullOrEmpty(colY)) {
	    		// default weight is 1.0
	    		colY = "1.0";
	    		yIsNumber = true;
	    	}
        	
        	// searching for xLeft and xRight boundaries for every color
        	int rowsFailed = 0;
        	for (Row row : getDataIter()) {
        		if (rowsFailed > 100) {
        			LibsLogger.error(PlotHistogramEntry.class, "Over 100 rows failed to compute for histogram, "
        					+ "breaking preparing the plot");
        			break;
        		}
        		
        		try {
	        		Double x = null;
	        		
	        		Object split = isSplitByDefined ? row.get(splitBy) : NOSPLIT;
	        		
					if (xNeedsComputing) {
						BigDecimal bc = row.compute(getX(), "$");
						x = bc != null ? bc.doubleValue() : null;
					} else
						x = row.getAsDouble(getX());
					
					if (x != null) {
		        		Object color = isColorByDefined ? row.get(colorBy) : colX;
		        		if (histogramLR.get(split, color, "L") == null) {
		        			histogramLR.put(split, color, "L", x);
		        			histogramLR.put(split, color, "R", x);
		            		minXGlobal = x;
		            		maxXGlobal = x;
		        		} else {
			        		if (x < ((double) histogramLR.get(split, color, "L")))
			        			histogramLR.put(split, color, "L", x);
			        		if (x > ((double) histogramLR.get(split, color, "R")))
			        			histogramLR.put(split, color, "R", x);
			        		minXGlobal = Math.min(minXGlobal, x);
			        		maxXGlobal = Math.max(maxXGlobal, x);
	        			}
					}
        		} catch (Throwable e) {
        			if (++rowsFailed < 10)
        				LibsLogger.error(PlotHistogramEntry.class, "Computing histogram for row " + row + " failed", e);
        		}
        	}
        	
        	// computing bin width
        	if (binWidth == 0.0) {
        		binWidth = (maxXGlobal - minXGlobal) / 10.0; // 10 bins by default
        	}
        		
//        	final List<String> params = getSeries().get(0).getParams();
			for (Row row : getDataIter()) {
				if (row == null)
					continue;
				
//				Object x = DataUtils.compute(colX, params, row);
//				Object y = DataUtils.compute(colY, params, row);
				
				Double x = null;
				Double weight = null;

				if (xNeedsComputing) {
					BigDecimal bc = row.compute(getX(), "$");
					x = bc != null ? bc.doubleValue() : null;
				} else
					x = row.getAsDouble(getX());
				
				if (yIsNumber)
					weight = Double.parseDouble(colY);
				else {
					if (yNeedsComputing)
						weight = row.compute(getY(), "$").doubleValue();
					else
						weight = row.getAsDouble(getY());
				}

        		Object split = isSplitByDefined ? row.get(splitBy) : NOSPLIT;
        		
				Object color = colorBy != null ? row.get(colorBy) : colX;
				
				if (x != null && weight != null && color != null) {
//						int bin = (int) ((x - histogramLR.get(color, "L")) / binWidth);
					int bin = (int) ((x - minXGlobal) / binWidth);
					histogram.put(split, color, bin, ((double) histogram.get(split, color, bin)) + (double) weight);
//					binValues.add(x);
					
					maxBin = Math.max(maxBin, bin);
				}
			}
			
			// saving histogram for later
			FileExt histH5 = getHistogramFile();
			
			if (new FileExt(histH5.getDirectoryOnly()).exists() == false)
				new FileExt(histH5.getDirectoryOnly()).mkdirs();
			
			if (histH5.exists())
				histH5.delete();
			
			H5Connector conn = new H5Connector(getId(), histH5);

			boolean firstRow = true;
			for (Object split : histogram.keySet()) {
				for (Object color : histogram.get(split).keySet()) {
					for (Integer bin : histogram.get(split).get(color).keySet()) {
						if (firstRow) {
							firstRow = false;
							ColumnDefList columns = new ColumnDefList();
							columns.add(new ColumnDef("split", "split name", ColumnDef.determineType(split)));
							columns.add(new ColumnDef("color", "color name", ColumnDef.determineType(color)));
							columns.add(new ColumnDef("bin", "bin", ColumnType.INTEGER));
							columns.add(new ColumnDef("value", "value", ColumnType.DOUBLE));
							columns.add(new ColumnDef("L", "L", ColumnType.DOUBLE));
							columns.add(new ColumnDef("R", "R", ColumnType.DOUBLE));
							conn.setColumnDefs(columns);
						}
						Row row = new Row();
						row.set("split", split);
						row.set("color", color);
						row.set("bin", bin);
						row.set("value", histogram.get(split).get(color).get(bin));
						row.set("L", histogramLR.get(split, color, "L"));
						row.set("R", histogramLR.get(split, color, "R"));
						conn.write(row);
					}
				}
			}
			
			conn.close();
			
//			if (binValues.size() > 0 && binValues.get(0) instanceof Number)
//				Collections.sort(binValues);
			
//			// histogram plots can handle only some limited number of bins
//			if (binValues.size() > BeansConst.PLOT_MAX_BAR_COUNT) {
//				LibsLogger.error(PlotHistogramEntry.class, "There is too many bars (", binValues.size() , ") for a plot, "
//						+ "browser will most likely not be able to plot that. Removing last bars...");
//				
//				// removing last bins
//				while (binValues.size() > BeansConst.PLOT_MAX_BAR_COUNT)
//					binValues.remove(binValues.size() - 1);
//			}
			
//			// prepare legend
//			for (Object colorValue : histogram.keySet()) {
//				if (colorValue != null) {
//					String legend = "";//plotSeries.getLegend();
//					
//					Query filter = new Query();
//					if (getSplitBy() != null && colorBy != null) {
//						filter
//							.addTermEQ(getSplitBy(), splitValue)
//							.addTermEQ(getColorBy(), colorValue);
//						legend += colorValue + " " + splitValue;
//					} else if (colorBy != null) {
//						filter
//							.addTermEQ(getColorBy(), colorValue);
//						legend += colorValue;
//					} else if (getSplitBy() != null && colorBy == null) {
//						filter
//							.addTermEQ(getSplitBy(), splitValue);
//						legend += splitValue;
//					} else {
//						filter = null;
//					}
//					
//					Row firstRow = plotData
//							.iterator(filter, 0, 0)
//							.next();
//
//					legend = ApplyParams.applyParams(legend, firstRow);
//					
//					colorToLegend.put(colorValue.toString(), legend);
//				}
//			}
    	} catch (Throwable t) {
    		LibsLogger.error(PlotHistogramEntry.class, "Cannot prepare data for histogram, returning no points", t);
    					
			return;
    	}
    	
    	// saving histogram
		
//		// writing the output data for nvd3 plot
//		Writer writer = new OutputStreamWriter(output);
//		writer.write(format("{ \"title\" : \"%s\", \"data\" : [ ", plotTitle));
//		
//		int colorCounter = 0;
//		for (Object color : histogram.keySet()) {
//			if (colorCounter > 0)
//				writer.write(", ");
//			
//			writer.write(format("{ \"key\": \"%s\",\"color\" : \"#%s\", \"values\": [ ", 
//					colorToLegend.get(color) != null ? colorToLegend.get(color) : (color != null ? color.toString() : ""), 
//					getPalette().toCssColor(colorCounter)));
//							
//			boolean first = true;
//			for (int bin = 0; bin <= maxBin; bin++) {
//				
////				}
////				for (int bin : histogram.get(color).keySet()) {
//				if (!first)
//					writer.write(",");
//				
////					double x = histogramLR.get(color, "L") + binWidth * bin;
//				double x = minXGlobal + binWidth * bin;
//				
//				writer.write(format("{ \"x\": %s, \"y\": %s }", String.valueOf(x), histogram.get(color, bin)));
//				first = false;
//			}
//			
//			writer.write("] }");
//			
//			colorCounter++;
//		}
//		
//		writer.write("] }");
//		
//		writer.flush();
//		writer.close();
	}
	
	public FileExt getHistogramFile() {
		return new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "/plugins/histograms/",
				"histogram-" + getId() + ".h5");
	}

	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.plots.PlotHistogramPanel";
	}


	public String getX() {
		return getMetaAsString(META_X_COLUMN, null);
	}

	public PlotHistogramEntry setX(String x) {
		setMeta(META_X_COLUMN, x);
		return this;
	}
	
	private List<UUID> getReadTables() {
		if (readTables == null) {
			String tbStr = getMetaAsString(META_READ_TABLES, null);
			if (tbStr == null) {
				readTables = new ArrayList<>();
			} else {
				readTables = JsonUtils.readList(tbStr, UUID.class);
			}
		}
		return readTables;
	}

	public void setReadTables(List<UUID> readTables) {
		setMeta(META_READ_TABLES, JsonUtils.objectToStringPretty(readTables));
	}

	public double getBinWidth() {
		return getMetaAsDouble(META_BINWIDTH, 0.0);
	}

	public void setBinWidth(double binWidth) {
		setMeta(META_BINWIDTH, binWidth);
	}

	public String getXLabel() {
		return getMetaAsString(META_XLABEL, null);
	}

	public void setXLabel(String xLabel) {
		setMeta(META_XLABEL, xLabel);
	}

	public String getYLabel() {
		return getMetaAsString(META_YLABEL, null);
	}

	public void setYLabel(String yLabel) {
		setMeta(META_YLABEL, yLabel);
	}

	public String getSplitBy() {
		return getMetaAsString(META_SPLITBY, null);
	}

	public void setSplitBy(String splitBy) {
		setMeta(META_SPLITBY, splitBy);
	}

	public String getColorBy() {
		return getMetaAsString(META_COLORBY, null);
	}

	public void setColorBy(String colorBy) {
		setMeta(META_COLORBY, colorBy);
	}

	public String getY() {
		return getMetaAsString(META_Y_COLUMN, null);
	}

	public void setY(String y) {
		setMeta(META_Y_COLUMN, y);
	}
	
	@Override
	public void stop() throws ValidationException, IOException {
		// TODO Auto-generated method stub	
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public StreamingOutput getDataAsStream(String split, String type) throws IOException {
		final ColorPalette palette = ColorPalette.getColorPalette(getUserId(), ColorPalette.defaultPalette());
		return new BarStreamingOutput(this, split, palette);
	}
}
